module.exports = function (grunt) {

  var livereload = require('connect-livereload'),
      path = require('path');

  require('load-grunt-tasks')(grunt);

  var paths = {
    dist: 'dist'
  };

  grunt.initConfig({
    paths: paths,
    connect: {
      options: {
        port: 3000,
        hostname: '0.0.0.0'
      },
      livereload: {
        options: {
          middleware: function (connect) {
            return [
              livereload({port: 35729}),
              connect.static(path.resolve('.tmp')),
              connect.static(path.resolve('./'))
            ];
          }
        }
      }
    },
    watch: {
      options: {
        livereload: 35729
      },
      compass: {
        files: [
          'scripts/**/*.less',
        ],
        tasks: ['less']
      },
      images: {
        files: [
          'scripts/**/*.{js,html}',
          '*.html',
          'images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },
    clean: {
      options: {
        force: true
      },
      temp: {
        src: ['.tmp']
      },
      dist: {
        src: ['<%= paths.dist %>']
      }
    },
    useminPrepare: {
      src: ['index.html'],
      options: {
        dest: '<%= paths.dist %>'
      }
    },
    less: {
      dist: {
        files: {
          ".tmp/scripts/content/layout.css": "scripts/content/layout.less"
        }
      }
    },
    requirejs: {
      // Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
      options: {
        name: 'main',
        baseUrl: 'scripts',
        mainConfigFile: 'scripts/main.js',
        out: '.tmp/scripts/main.js',
        optimize: 'none',
        almond: true,
        preserveLicenseComments: false,
        useStrict: true,
        wrap: true,
        findNestedDependencies: true
      },
      dist: {}
    },
    ngAnnotate: {
      options: {
        singleQuotes: true
      },
      dist: {
        files: {
          '.tmp/scripts/main.js': ['.tmp/scripts/main.js']
        }
      }
    },
    autoprefixer: {
      dist: {
        expand: true,
        src: '.tmp/**/*.css'
      }
    },
    htmlmin: {
      dist: {
        options: {
          //removeCommentsFromCDATA: true,
          // https://github.com/yeoman/grunt-usemin/issues/44
          collapseWhitespace: true
          //collapseBooleanAttributes: true,
          //removeAttributeQuotes: true,
          //removeRedundantAttributes: true,
          //useShortDoctype: true,
          //removeEmptyAttributes: true,
          //removeOptionalTags: true
        },
        files: [
          {
            expand: true,
            cwd: '<%= paths.dist %>',
            src: '*.html',
            dest: '<%= paths.dist %>'
          }
        ]
      }
    },
    imagemin: {
      dist: {
        files: [
          {
            expand: true,
            cwd: 'images',
            src: ['**/*.{png,jpg,gif}'],
            dest: '<%= paths.dist %>/images'
          }
        ]
      }
    },
    uglify: {
      options: {
        compress: {
          sequences: true,
          dead_code: true,
          drop_debugger: true,
          comparisons: true,
          conditionals: true,
          evaluate: true,
          booleans: true,
          loops: true,
          if_return: true,
          join_vars: true,
          drop_console: true
        }
      },
      requirejs: {
        files: {
          '<%= paths.dist %>/scripts/main.js': [
            '.tmp/scripts/main.js'
          ],
          '<%= paths.dist %>/vendor/requirejs/require.js': [
            'vendor/requirejs/require.js'
          ]
        }
      }
    },
    copy: {
      dist: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: '.',
            dest: '<%= paths.dist %>',
            src: [
              '*.{ico,txt}',
              'index.html'
            ]
          }
        ]
      }
    },
    filerev: {
      dist: {
        src: [
          '<%= paths.dist %>/scripts/**/*.js',
          '<%= paths.dist %>/styles/**/*.css',
          '<%= paths.dist %>/vendor/**/*.js'
        ]
      }
    },
    usemin: {
      html: ['<%= paths.dist %>/{,*/}*.html'],
      css: ['<%= paths.dist %>/styles/{,*/}*.css'],
      options: {
        dirs: ['<%= paths.dist %>']
      }
    }
  });

  grunt.registerTask('serve', [
    'clean:temp',
    'less',
    'connect:livereload',
    'watch'
  ]);

  grunt.registerTask('build', [
    'clean',
    'useminPrepare',
    'requirejs',
    'less',
    'concat',
    'ngAnnotate',
    'autoprefixer',
    'imagemin',
    'cssmin',
    'uglify',
    'copy',
    'filerev',
    'usemin',
    'htmlmin'
  ]);
};