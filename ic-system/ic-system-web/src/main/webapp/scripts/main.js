﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija orodij in knjižnjic, njihovih osnovnih odvisnosti in inicializacija aplikacije.
 */
require.config({
  waitSeconds: 0,
  paths: {
    'jQuery': '../vendor/jquery/dist/jquery',
    'lodash': '../vendor/lodash/lodash',
    'angular': '../vendor/angular/angular',
    'angular-route': '../vendor/angular-route/angular-route',
    'angular-ui-router': '../vendor/angular-ui-router/release/angular-ui-router',
    'angular-sanitize': '../vendor/angular-sanitize/angular-sanitize',
    'angular-resource': '../vendor/angular-resource/angular-resource',
    'angular-maps': '../vendor/angular-maps/angular-google-maps.min',
    'angular-local-storage': '../vendor/angular-local-storage/angular-local-storage.min',
    'async': '../vendor/async/async',
    'ngTable': '../vendor/ng-table/dist/ng-table',
    'i18n': '../vendor/requirejs-i18n/i18n',
    'text': '../vendor/requirejs-text/text',
    'bootstrap': '../libs/bootstrap/bootstrap.min',
    'ui-bootstrap': '../libs/bootstrap/ui-bootstrap-tpls-0.12.0',
    'jQuery-ui': '../vendor/jquery-ui/jquery-ui',
    'smart-notification': '../libs/notification/SmartNotification.min',
    'smart-widgets': '../libs/smartwidgets/jarvis.widget.min',
    'cookie-choices': '../libs/cookie-choices/cookiechoices',
    'charts': '../vendor/chart/Chart',
    'keycloak': '../vendor/keycloak/keycloak',
    'jspdf' : '../vendor/jspdf/jspdf.min',
    'blob' : '../vendor/jspdf/Blob',
    'file-saver': '../vendor/jspdf/FileSaver',
    'jspdf-tables': '../vendor/jspdf/jspdf.plugin.table'

  },
  packages: [
    {
      name: 'less',
      location: '../vendor/require-less',
      main: 'less'
    },
    {
      name: 'css',
      location: '../vendor/require-css',
      main: 'css'
    }
  ],
  shim: {
    'angular': {
      exports: 'angular',
      deps: [ 'jQuery' ]
    },
    'angular-route': {
      deps: [ 'angular' ]
    },
    'angular-ui-router': {
      deps: [ 'angular' ]
    },
    'angular-sanitize': {
      deps: [ 'angular' ]
    },
    'angular-resource': {
      deps: [ 'angular']
    },
    'angular-local-storage': {
      deps: ['angular']
    },
    'angular-maps': {
      deps: ['angular', 'lodash']
    },
    'ngTable': {
      deps: [ 'angular' ]
    },
    'jQuery': {
      exports: '$'
    },
    'jQuery-ui': {
      deps: [ 'jQuery' ]
    },
    'bootstrap': {
      deps: [ 'jQuery' ]
    },
    'smart-notification': {
      deps: [ 'jQuery' ]
    },
    'smart-widgets': {
      deps: [ 'jQuery-ui' ]
    },
    'ui-bootstrap': {
      deps: [ 'angular' ]
    },
    'cookie-choices': {
      exports: 'cookieChoices'
    },
    'keycloak': {
      exports: 'Keycloak'
    },
    'jspdf':{
      deps: [
        'blob', 
        'file-saver', 
        'jQuery' ]
    },
    'jspdf-tables': {
      deps:[ 'jspdf' ]
    }
  },
  config: {
    'less/less': {
      less: {
        env: 'development', // or 'production'
        //async: false,       // load imports async
        //fileAsync: false,   // load imports async when in a page under

        functions: {},      // user functions, keyed by name
        dumpLineNumbers: 'mediaQuery', // or 'mediaQuery' or 'all'
        relativeUrls: false,// whether to adjust url's to be relative: if false, url's are already relative to the
                            // entry less file
        rootpath: '/src'// a path to add on to the start of every url resource
      }
    }
  }
} );

require( [
  'app'
], function ( App ) {
  App.initialize();
} );
