﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Gradnja drevesa stanj podstrani spletne aplikacije.
 *
 * @ngdoc object
 */
define([
  'lodash', '../content/states'
], function ( _, states ) {
  var StateTree = function ( _states ) {
    if ( !_states ) throw new Error( '_states is null or undefined in stateTree' );
    this.defaultPath = _states.defaultPath;
    this.root = _states;
  };

  var StateTreeNode = function () {
  };

  StateTreeNode.prototype.getParentSegment = function () {
    var node = this;
    while ( node.parent && !node.isSegment ) {
      node = node.parent;
    }
    return node;
  };

  var addStateRecursive = function ( root, stateProvider, currentId, currentUrl, currentSegment, depth ) {
    if ( !depth ) depth = 0;
    var id = currentId ? currentId + '.' + root.id : root.id;

    root.depth = depth;
    root.absoluteId = id;
    root.absoluteUrl = currentUrl ? currentUrl + root.url : '!' + root.url;

    currentUrl = root.absoluteUrl;

    if ( !root.isSubPage ) root.isSubPage = false;
    else root.isSubPage = true;

    if ( !root.hideSubPages ) root.hideSubPages = false;
    else root.hideSubPages = true;

    if ( root.visible !== false ) root.visible = true;

    if ( root.isSegment !== true ) root.isSegment = false;

    if ( root.isSegment ) {
      currentSegment = root;
    }
    root.segment = currentSegment;

    stateProvider.state( id, root );

    if ( root.children ) {

      _.forEach( root.children, function ( state, i ) {

        state.parentState = root;
        state.menuParentState = findFirstMenuParent( root );
        if ( state.menuParentState === state.parentState ) {
          state.menuIndex = i;
        } else {
          state.menuIndex = state.parentState.menuIndex;
        }

        // set child authentication requirements
        if ( (typeof state.requiresAuth === 'undefined' &&
          typeof root.requiresAuth !== 'undefined') || root.requiresAuth ) {
          state.requiresAuth = root.requiresAuth;
        }

        // set child roles requirements
        if ( typeof state.requiredRoles === 'undefined' &&
          typeof root.requiredRoles !== 'undefined' ) {
          state.requiredRoles = _.clone( root.requiredRoles );
        } else if ( state.requiredRoles && root.requiredRoles ) {
          state.requiredRoles = _.uniq( state.requiredRoles.concat( root.requiredRoles ) );
        }

        addStateRecursive( state, stateProvider, id, currentUrl, currentSegment, depth + 1 );
      } );
    }
  };

  var findFirstMenuParent = function ( root ) {
    if ( root.isMenuParent !== false ) {
      return root;
    } else if ( root.parentState ) {
      return findFirstMenuParent( root.parentState );
    }
    return null;
  };

  StateTree.prototype.addStates = function ( stateProvider ) {
    if ( !this.root ) return;
    addStateRecursive( this.root, stateProvider );
  };

  return new StateTree( states );
} );