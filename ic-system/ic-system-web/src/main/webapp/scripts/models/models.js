/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Inicializacija vseh podanih modelov
 *
 * @ngdoc object
 */
define([
  'lodash'
], function ( _ ) {

  var models = {
  };

  var initialize = function ( angModule ) {
    _.forEach( models, function ( model, name ) {
      angModule.factory( name, model );
    } );
  };

  return {
    initialize: initialize
  };
} );