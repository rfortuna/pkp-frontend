/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za ra�unalni�tvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Inicializira vse podane factoryje.
 *
 * @ngdoc object
 */
define([
  'lodash', './utils'
], function ( _, Utils) {

  var factories = {
    $utils: Utils
  };

  var initialize = function ( angModule ) {
    _.forEach( factories, function ( factory, name ) {
      angModule.factory( name, factory );
    } );
  };

  return {
    initialize: initialize
  };
} );