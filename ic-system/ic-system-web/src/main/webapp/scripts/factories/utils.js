/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Factory za prikaz obvestil o napakah, uspehih in opozorilih.
 *
 * @ngdoc function
 */
define([
  'jQuery'
], function ( $ ) {
  return function () {

    return {
      showError: function ( title, content, timeout ) {
        $.smallBox( {
          title: '<h3 style="margin-bottom:10px">' + title + '</h3>',
          content: content,
          color: '#d9534f',
          iconSmall: 'fa fa-frown-o bounce animated',
          timeout: (timeout || 5000)
        } );

        $( '.SmallBox' ).addClass( 'http-error' );
      },

      showSuccess: function ( title, content, timeout ) {
        $.smallBox( {
          title: '<h3 style="margin-bottom:10px">' + title + '</h3>',
          content: content,
          color: '#5cb85c',
          iconSmall: 'fa fa-smile-o bounce animated',
          timeout: (timeout || 5000)
        } );

        $( '.SmallBox' ).addClass( 'http-success' );
      },

      showWarning: function ( title, content, timeout ) {
        $.smallBox( {
          title: '<h3 style="margin-bottom:10px">' + title + '</h3>',
          content: content,
          color: '#C79121',
          iconSmall: 'fa fa-smile-o bounce animated',
          timeout: (timeout || 5000)
        } );

        $( '.SmallBox' ).addClass( 'http-success' );
      }
    }
  };
} );