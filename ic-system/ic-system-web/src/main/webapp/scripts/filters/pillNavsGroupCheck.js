/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za ra�unalni�tvo in informatiko Univerze v
 *   Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Factory za prikaz obvestil o napakah, uspehih in opozorilih.
 *
 * @ngdoc function
 */
define( [], function () {
  return function () {

    return function ( data, filter ) {
      var input = [];
      
    	for(var i = 0; i < data.length; i++){
        if(filter == "" || data[i].tip == filter){
           input.push(data[i]);
        }
      }
      return input;
    }

  };
} );