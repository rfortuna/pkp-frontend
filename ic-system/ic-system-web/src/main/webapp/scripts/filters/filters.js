﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v
 *   Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Inicializira vse podane filtre.
 *
 * @ngdoc object
 */
define( [
  'lodash', './capitalize' , './pillNavsGroupCheck'
], function ( _, capitalize, pillNavsGroupCheck ) {

  var filters = {
    capitalize: capitalize,
    pillNavsGroupCheck: pillNavsGroupCheck
  };

  var initialize = function ( angModule ) {
    _.forEach( filters, function ( filter, name ) {
      angModule.filter( name, filter );
    } );
  };
  return {
    initialize: initialize
  };
} );