/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za ra�unalni�tvo in informatiko Univerze v
 *   Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Factory za prikaz obvestil o napakah, uspehih in opozorilih.
 *
 * @ngdoc function
 */
define( [
  'lodash'
], function ( _ ) {
  return function () {

    return function ( input ) {

      if ( input != null )
        input = input.toLowerCase();
      else
        return '';

      var inputs = _( input.split( ' ' ) ).map( function ( i ) {
        return i.substring( 0, 1 ).toUpperCase() + i.substring( 1 );
      } );

      return inputs.join( ' ' );
    }
  };
} );