﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Direktiva za prikaz navigacijskega menija.
 *
 * @ngdoc directive
 */
define([
  'lodash', 'jQuery'
], function ( _ , $) {

  return /*@ngInject*/ function ( $compile, $rootScope ) {

    return {
      restrict: 'E',
      transclude: true,
      link: function ( scope, element ) {
        var rootState = scope.$stateTree.root;

        var generateHtmlForState = function ( state ) {
          var tempLi, li;

          var title = state.strings.title;

          // if children, abstract class, no children in menu
          if ( state.children && state.children.length > 0 && state.abstract && state.hideSubPages === true ) {
            tempLi = "<li ng-class='{active:isActiveState(\"" + state.absoluteId + "\")}'>" +
            "<a ng-href='" + scope.getStateUrl( state.children[ 0 ].absoluteId ) + "' ng-title='" +
            title + "'>" + "<i class='fa fa-lg fa-fw " + state.icon + "'></i>" +
            "<span class='menu-item-parent'>&nbsp;" + title + "</span>" + "</a></li>";
            li = $compile( tempLi )( scope );
            return li;
          }
          //if children
          if ( state.children && state.children.length > 0 && state.abstract ) {
            var activeChild = scope.hasActiveChild( state ) || state.openedOnNoAuth;
            tempLi = (activeChild ? "<li class='open'>" : "<li>") +
            "<a style='cursor: pointer;' ng-href='' ng-title='" + title + "'>" +
            "<i class='fa fa-lg fa-fw " + state.icon + "'></i>" +
            "<span class='menu-item-parent'>&nbsp;" + title + "</span></a></li>";
            li = $compile( tempLi )( scope );
            li.append( generateHtml( state.children, activeChild ) );
            return li;
          }
          //else if no children
          else {
            tempLi = "<li ng-class='{active:isActiveState(\"" + state.absoluteId + "\")}'>" +
            "<a ng-href='" + scope.getStateUrl( state.absoluteId ) + "' ng-title='" + title + "'>" +
            "<i class='fa fa-lg fa-fw " + state.icon + "'></i>" +
            "<span class='menu-item-parent'>&nbsp;" + title + "</span>" +
            "</a></li>";
            li = $compile( tempLi )( scope );
            return li;
          }
        };

        var checkRolesRecursive = function ( state ) {
          var allowed = !state.requiredRoles ||
            _.every( state.requiredRoles, function ( role ) {
              if($.inArray(role, $rootScope.keycloak.realmAccess.roles) > - 1 ){
                return true;
              }

            } );
          var allowedChildes = true;

          if ( state.children && state.children.length > 0 ) {
            allowedChildes = false;
            _.forEach( state.children, function ( child ) {
              allowedChildes = allowedChildes || checkRolesRecursive( child );
            } );
          }

          return allowed && allowedChildes;
        };

        var checkVisibilityRecursive = function ( state ) {
          var visible = state.visible === true;
          var visibleChildes = true;

          if ( state.children && state.children.length > 0 ) {
            visibleChildes = false;
            _.forEach( state.children, function ( child ) {
              visibleChildes = visibleChildes || checkVisibilityRecursive( child );
            } );
          }

          return visible && visibleChildes;
        };

        generateHtml = function ( states, active ) {
          var tempUl = active ? "<ul style='display: block;'></ul>" : "<ul></ul>";
          var ul = $compile( tempUl )( scope );

          states.forEach( function ( state ) {
            //if roles are ok && isVisible && noParams
            if ( (!state.requiresAuth || true /* check auth here */ ) &&
              checkRolesRecursive( state ) &&
              checkVisibilityRecursive( state ) && state.hasParams !== true )
              ul.append( generateHtmlForState( state ) );
          } );
          return ul;
        };

        //build navigation
        var tempNav = "<nav></nav>";
        var nav = $compile( tempNav )( scope );
        nav.append( generateHtml( rootState.children ) );

        //append navigation
        element.append( nav );

        //initalize jarvismenu
        element.first().jarvismenu( {
          accordion: true,
          speed: $.menu_speed,
          closedSign: '<em class="fa fa-plus-square-o"></em>',
          openedSign: '<em class="fa fa-minus-square-o"></em>'
        } );
      }
    }
  };
} );