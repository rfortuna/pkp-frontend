define([], function () {

  return function () {

    return {
      restrict: 'E',
      replace: true,
      templateUrl: "./scripts/directives/widgetBarChart/barChart.html",
      scope: {
    	  podatkichart: "=",
    	  index: "=",
      },
      controller: function($scope, $sce, $filter) {

		var chart;
  		var ctx = $("canvas").get($scope.index).getContext("2d");

		var generateLegendHtml = function(data) {
    		var html = '';
    		
    		html += '<ul class=\"chartLegend\">';
    		var i = 0;
    		angular.forEach(data.datasets, function(obj){
    			html += '<li>';
    			var box = '<span style=\"background-color: ' + obj.fillColor + ';\" class=\"legendBox\" ></span>';
    			html += box;
    			html += '<span class=\"legendText\">';
    			html += obj.label;
    			html += '</span>';
    			html += '</li>';
    			i++;
    		});
    		html += '</ul>';
    		return html;
    	} 
    	
    	var createLegend = function(data) {
			var legendHolder = $(".legendHolder").get($scope.index);
			legendHolder.innerHTML = generateLegendHtml(data);
		
    	}
    	
		
		var narisiGraf = function() {
			if(chart != undefined) chart.destroy();
				chart = new Chart(ctx).Bar($scope.podatkichart);
				createLegend($scope.podatkichart);
			
	
		}
		

		$scope.$watch('$includeContentLoaded', function(event) {
			narisiGraf();
	    });
    	
		$scope.$watch('podatkichart', function(){
			narisiGraf();
		});
		
		
      }
    }
  };
});




