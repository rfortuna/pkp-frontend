﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Direktiva za prikaz jarviswidgeta.
 *
 * @ngdoc directive
 */
define([], function () {

  return function () {

    return {
      restrict: 'AE',
      link: function ( scope, element ) {
        scope.setup_widget_desktop = function () {
          if ( $.enableJarvisWidgets ) {
            element.jarvisWidgets( {
              grid: 'article',
              widgets: '.jarviswidget',
              localStorage: false,
              deleteSettingsKey: '#deletesettingskey-options',
              settingsKeyLabel: 'Reset settings?',
              deletePositionKey: '#deletepositionkey-options',
              positionKeyLabel: 'Reset position?',
              sortable: true,
              buttonsHidden: false,
              // toggle button
              toggleButton: true,
              toggleClass: 'fa fa-minus | fa fa-plus',
              toggleSpeed: 200,
              onToggle: function () {
              },
              // delete btn
              deleteButton: false,
              deleteClass: 'fa fa-times',
              deleteSpeed: 200,
              onDelete: function () {
              },
              // edit btn
              editButton: false,
              editPlaceholder: '.jarviswidget-editbox',
              editClass: 'fa fa-cog | fa fa-save',
              editSpeed: 200,
              onEdit: function () {
              },
              // color button
              colorButton: false,
              // full screen
              fullscreenButton: true,
              fullscreenClass: 'fa fa-expand | fa fa-compress',
              fullscreenDiff: 3,
              onFullscreen: function () {
                scope.$emit( 'widgetFullscreenToggle' );
              },
              // custom btn
              customButton: false,
              customClass: 'folder-10 | next-10',
              customStart: function () {
                alert( 'Hello you, this is a custom button...' );
              },
              customEnd: function () {
                alert( 'bye, till next time...' );
              },
              // order
              buttonOrder: '%refresh% %custom% %edit% %toggle% %fullscreen% %delete%',
              opacity: 1.0,
              dragHandle: '> header',
              placeholderClass: 'jarviswidget-placeholder',
              indicator: true,
              indicatorTime: 600,
              ajax: true,
              timestampPlaceholder: '.jarviswidget-timestamp',
              timestampFormat: 'Last update: %m%/%d%/%y% %h%:%i%:%s%',
              refreshButton: true,
              refreshButtonClass: 'fa fa-refresh',
              labelError: 'Sorry but there was a error:',
              labelUpdated: 'Last Update:',
              labelRefresh: 'Refresh',
              labelDelete: 'Delete widget:',
              afterLoad: function () {
              },
              rtl: false, // best not to toggle this!
              onChange: function () {

              },
              onSave: function () {

              },
              ajaxnav: $.navAsAjax // declears how the localstorage should be saved (HTML or AJAX page)

            } );
          }
        };

        scope.setup_widget_mobile = function () {
          if ( $.enableMobileWidgets && $.enableJarvisWidgets ) {
            scope.setup_widget_desktop();
          }
        };

        if ( $.device === 'desktop' ) {
          scope.setup_widget_desktop();
        } else {
          scope.setup_widget_mobile();
        }

      }
    }
  };
} );