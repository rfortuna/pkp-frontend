/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Direktiva za validacijo forme ob submitu.
 *
 * @ngdoc directive
 */
define([
  'jQuery', 'lodash'
], function ($, _) {

  return function () {

    return {
      require: 'form',
      restrict: 'E',
      link: function( scope , element , attributes ){
        var $element = $(element);

        $element.on('submit', function() {

          $element.find('.ng-pristine').removeClass('ng-pristine').addClass('ng-dirty');
          var form = scope[ attributes.name ];
          _.forEach( form , function ( formElement , fieldName ) {
            if ( fieldName[0] === '$' ) return;

            formElement.$pristine = false;
            formElement.$dirty = true;
          }, this);

          form.$setDirty();
          scope.$apply();
        });
      }
    }
  };
});