﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Direktiva za prikaz jarviswidgeta.
 *
 * @ngdoc directive
 */
define([], function () {

  return function () {

    return {
      restrict: 'AE',
      transclude: true,
      replace: true,
      template: '<div data-ng-transclude=""></div>'
    }
  };
} );