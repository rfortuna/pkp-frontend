
define([ 'lodash'
], function ( _) {

  return function () {

    return {
      restrict: 'E',
      replace: true,
      templateUrl: "./scripts/directives/widgetTransactionsInfoTable/transactionsInfoTable.html",
      scope: {
    	  transakcije: "=",
    	  podrobnostitransakcije: "=",
    	  naslovitabele: "=",
        prikazivrsto: "=",
        loaddata: "=",
        sort: "="

      },
      controller: function($scope, prometService) {
		    
        $scope.preuredi = function(podatek) {
          //prejsnji sort po enakem fieldu kot trenutni
          if($scope.sort.sortirajPo == podatek){
            if($scope.sort.ascOrDesc == "ASC"){
              $scope.sort.ascOrDesc = "DESC";
            }else{
              $scope.sort.ascOrDesc = "ASC";
            }
          }else{
            $scope.sort.sortirajPo = podatek;
          }
          $scope.loaddata();
		    }
		    
		 $scope.prikaziPodrobnosti = function(transakcija) {
		   $scope.podrobnostitransakcije.podrobnosti = transakcija;
	       $scope.podrobnostitransakcije.prikazi = true;
		 }

      }
    }
  };
});