
define([
	'i18n!./nls/transactionDetails', 'lodash'
], function (strings, _) {

  return function () {

    return {
      restrict: 'E',
      replace: true,
      templateUrl: "./scripts/directives/widgetTransactionDetails/transactionDetails.html",
      scope: {
    	  podrobnostitransakcije: "="
      },
      controller: function($scope) {
    	  
    	 $scope.$strings = _.merge( {}, strings, $scope.$strings );

       
    	 $scope.skrijPodrobnosti = function() {
    		 $scope.podrobnostitransakcije.prikazi = false;
    	 }

      }
    }
  };
});