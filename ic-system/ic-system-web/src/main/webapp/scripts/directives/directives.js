﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Inicializira vse podane direktive.
 *
 * @ngdoc object
 */
define([
  'lodash', './action', './widget', './widgetBody', './widgetBodyToolbar', './widgetContent', './widgetEditbox',
  './widgetHeader', './widgetToolbar', './widgetGrid', './navLeft', './dynamicTemplateInclude', './ngMatch',
  './validator', './widgetDatePicker/widgetDatePicker', './widgetTransactionsInfoTable/widgetTransactionsInfoTable',
  './widgetPieChart/widgetPieChart', './widgetTransactionDetails/widgetTransactionDetails', './widgetPillNav/widgetPillNav',
  './widgetBarChart/widgetBarChart', './widgetStrankeDetails/widgetStrankeDetails'

], function ( _, action, widget, widgetBody, widgetBodyToolbar, widgetContent, widgetEditbox,
              widgetHeader, widgetToolbar, widgetGrid, navLeft, dynamicTemplateInclude, ngMatch,
              validator, widgetDatePicker, widgetTransactionsInfoTable,
              widgetPieChart, widgetTransactionDetails, widgetPillNav,
              widgetBarChart, widgetStrankeDetails, widgetStrankeList) {

  var directives = {
    action: action,
    widget: widget,
    widgetBody: widgetBody,
    widgetBodyToolbar: widgetBodyToolbar,
    widgetContent: widgetContent,
    widgetEditbox: widgetEditbox,
    widgetHeader: widgetHeader,
    widgetToolbar: widgetToolbar,
    widgetGrid: widgetGrid,
    navLeft: navLeft,
    dynamicTemplateInclude: dynamicTemplateInclude,
    ngMatch: ngMatch,
    form: validator,
    widgetDatePicker: widgetDatePicker,
    widgetTransactionsInfoTable: widgetTransactionsInfoTable,
    widgetPieChart: widgetPieChart,
    widgetTransactionDetails: widgetTransactionDetails,
    widgetPillNav: widgetPillNav,
    widgetBarChart: widgetBarChart,
    widgetStrankeDetails: widgetStrankeDetails
  };

  var initialize = function ( angModule ) {
    angular.forEach( directives, function ( directive, name ) {
      angModule.directive( name, directive );
    } );
  };

  return {
    initialize: initialize
  };
} );