/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Direktiva za prikaz jarviswidgeta.
 *
 * @ngdoc directive
 */
define(['i18n!./nls/widgetStrankeDetails', 'lodash'
    ], function (strings, _) {

  return function () {

    return {
      restrict: 'E',
      replace: true,
      templateUrl: "./scripts/directives/widgetStrankeDetails/strankeDetails.html",
      scope: {
      	activestranka: "="
      },
      controller: function($scope, $stateParams, $location, strankeService, $state) {
    	  
     	   $scope.$strings = _.merge( {}, strings, $scope.$strings );

     	   $scope.openStranka = function(){
     		  $state.go('^.stranka', {strankaId: $scope.activestranka.id});
     	   }
      }
    }
  };
} );