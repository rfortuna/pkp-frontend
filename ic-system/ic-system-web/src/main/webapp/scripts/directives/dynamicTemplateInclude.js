﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Direktiva za dinamično vključevanje templateov.
 *
 * @ngdoc directive
 */
define([
  'angular'
], function ( angular ) {

  return /*@ngInject*/ function ( $compile ) {

    return function ( scope, elem, attrs ) {

      var template = attrs.dynamicTemplateInclude;

      if ( (typeof template == 'string') && (template.charAt( 0 ) !== '<') ) {
        html = '<span>' + template + '</span>';
      } else if ( (typeof template == 'string') && (/<[a-z][\s\S]*>/i.test( template )) ) {
        html = '<div>' + template + '</div>';
      } else {
        html = template;
      }

      var el = angular.element( html ),
        compiled = $compile( el );

      elem.append( el );

      compiled( scope );
    };
  };
} );