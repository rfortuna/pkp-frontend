/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Direktiva za preverjanje ujemanja vnosa z drugim vnosom.
 *
 * @ngdoc directive
 */
define([], function () {

  return /*@ngInject*/ function ( $parse ) {

    return {
      restrict: 'A',
      require: '?ngModel',
      link: function ( scope, elem, attrs, ctrl ) {
        if ( !ctrl ) return;
        if ( !attrs[ 'ngMatch' ] ) return;

        var firstPassword = $parse( attrs[ 'ngMatch' ] );

        var validator = function ( value ) {
          var temp = firstPassword( scope ),
            v = value === temp;
          ctrl.$setValidity( 'match', v );
          return value;
        };

        ctrl.$parsers.unshift( validator );
        ctrl.$formatters.push( validator );
        attrs.$observe( 'ngMatch', function () {
          validator( ctrl.$viewValue );
        } );
      }
    }
  };
} );