define(['charts'], function (Chart) {

  return function () {

    return {
      restrict: 'E',
      replace: true,
      templateUrl: "./scripts/directives/widgetPieChart/pieChart.html",
      scope: {
    	  podatkichart: "=",
    	  index: "=",
    	  prikazi:"="
      },
      controller: function($scope, $sce) {
    	  
    	var helpers = Chart.helpers;
    	  
    	var options = {
    		responsive: false,
    	}

	  	var generateLegendHtml = function(data) {
    		var html = '';
    		
    		html += '<ul class=\"chartLegend\">';
    		
    		angular.forEach(data, function(obj){
    			html += '<li>';
    			var box = '<span style=\"background-color: ' + obj.color + ';\" class=\"legendBox\" ></span>';
    			html += box;
    			html += '<span class=\"legendText\">';
    			html += obj.naziv;
    			html += '</span>';
    			html += '</li>';
    		});
    		html += '</ul>';
    		return html;
    	} 
    	
    	var chart;
    	var createLegend = function(data) {
			var legendHolder = $(".legendHolder").get($scope.index);
			legendHolder.innerHTML = generateLegendHtml(data);
			
			helpers.each(legendHolder.firstChild.childNodes, function(legendNode, index){
			    helpers.addEvent(legendNode, 'mouseover', function(){
			        var activeSegment = chart.segments[index];
			        activeSegment.save();
			        activeSegment.fillColor = activeSegment.highlightColor;
			        chart.showTooltip([activeSegment]);
			        activeSegment.restore();
			    });
			});
			helpers.addEvent(legendHolder.firstChild, 'mouseout', function(){
			    chart.draw();
			});
    	}
    	
		
		var narisiGraf = function() {
			if(chart != undefined) chart.destroy();
			setTimeout(function(){
				var ctx = $("canvas").get($scope.index).getContext("2d");
				chart = new Chart(ctx).Pie($scope.podatkichart, options);
				createLegend($scope.podatkichart);
				
			},100);
			
		}

		$scope.$watch('$includeContentLoaded', function(event) {
			narisiGraf();
	    });
		
		$scope.$watch('podatkichart', function(){
			narisiGraf();
		});
		
		//za promet ob menjavi 
		$scope.$watch('prikazi', function(){
			narisiGraf();
		});
    	
      }
    }
  };
});