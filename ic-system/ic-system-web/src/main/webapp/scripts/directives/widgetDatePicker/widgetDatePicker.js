/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Direktiva za prikaz jarviswidgeta.
 *
 * @ngdoc directive
 */
define([], function () {

  return function () {

    return {
      restrict: 'E',
      replace: true,
      templateUrl: "./scripts/directives/widgetDatePicker/datePicker.html",
      scope: {
      	dt: "=",
      	title: "="
      },
      controller: function($scope) {
	  	  
    	  var today = function() {
    		  return new Date();
    	  }
    	  
    	  $scope.today = today();
    	  
		  $scope.clear = function () {
		    $scope.dt.date = null;
		  };
	
		  // Disable weekend selection
		  $scope.disabled = function(date, mode) {
		    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
		  };
	
		  $scope.open = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();
	
		    $scope.opened = true;
		  };
	
		  $scope.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1
		  };
      }
    }
  };
} );