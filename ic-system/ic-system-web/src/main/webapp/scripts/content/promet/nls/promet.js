/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Nizi za podstran zemljevid.
 *
 * @ngdoc object
 */
define({
  root: {
    content: 'Hello!',
    obdobjeOd: 'Obdobje od',
    obdobjeDo: 'do',
    nasloviTabelePrihodki: [ { 
    						naslov: 'Stranka',
							sort: "naziv"
							}, 
							{
								naslov: 'sn',
								sort:"sn"
							},
							{
								naslov: 'Datum',
								sort: "datumIzdaje"
							}
							, 
							{
								naslov: 'Znesek',
								sort:"znesek"
							}],

	nasloviTabeleOdhodki: [ { 
							naslov: 'Stranka',
							sort: "naziv"
							}, 
							{
								naslov: 'sn',
								sort:"sn"
							},
							{
								naslov: 'Datum',
								sort: "datumIzdaje"
							}
							, 
							{
								naslov: 'Znesek',
								sort:"znesek"
							}],
	alertNiRezultatov: 'Iskanje ni dalo rezultatov',
	iskanje: 'Iskanje'
    
  }
});
