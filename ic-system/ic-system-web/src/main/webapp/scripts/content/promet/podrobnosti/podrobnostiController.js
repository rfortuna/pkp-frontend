﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran namizje (namizje.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/podrobnosti', 'lodash'
], function ( strings, _ ) {

  return /*@ngInject*/ function ( $scope, $stateParams, $state, prometService) {
  	  		
    $scope.$strings = _.merge( {}, strings, $scope.$strings );
        
    $scope.podrobnostiTransakcije = {
    		podrobnosti: {},
    		prikazi: true
    }
    
    prometService.podrobnostiRacuna.get({
    	id: $stateParams.transakcijaId},
    	function(details){
    		$scope.podrobnostiTransakcije.podrobnosti = details;
    });
    
    $scope.$watch("podrobnostiTransakcije.prikazi", function() {
    	if($scope.podrobnostiTransakcije.prikazi == false) {
    		$state.go('^.promet');
    	}
    })
  };
} );