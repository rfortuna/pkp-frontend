﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija stanja aplikacije (id: 'namizje')
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './podrobnostiController', 'text!./podrobnosti.html'
], function ( strings, podrobnostiController, podrobnostiTemplate) {
  return {
    id: 'transakcija',
    url: strings.podrobnosti.url,
    strings: strings.podrobnosti,
    template: podrobnostiTemplate,
    controller: podrobnostiController,
    isSubPage: true,
    icon: 'fa-automobile',	
    requiresAuth: true,
    hasParams: true
  };
} );
