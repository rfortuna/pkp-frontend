﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Metpodatki za podstran zemljevid.
 *
 * @ngdoc object
 */
define({
  root: {
    promet: {
      url: '/promet?datumOd&datumDo',
      title: 'Promet',
      longTitle: 'Promet',
      description: 'Odhodki / Prihodki',
      keywords: 'promet, odhodki, prihodki'
      
    }
  }
} );
