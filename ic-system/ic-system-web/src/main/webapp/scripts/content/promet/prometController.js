﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za
 *         računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 * 
 * @description Kontroler za podstran zemljevid (zemljevid.html).
 * 
 * @ngdoc function
 * @scope
 */
define([ 'i18n!./nls/promet', 'lodash' ], function(strings, _) {

	return /* @ngInject */function($scope, $stateParams, $routeParams,
			$location, $state, prometService, porocilaStrukturaService) {
		$scope.$strings = _.merge({}, strings, $scope.$strings);

		$scope.prikazi = 0;

		var services = [ prometService.prihodki, prometService.odhodki ];

		// da se requesti ne pošiljajo po nepotrebnem ($watch, tabchanged itd. )
		$scope.isFirstLoad = true;
		

		// testDate
		var dateChange = function(newValue, oldValue) {

			changeDatesInUrl();
			if (!$scope.isFirstLoad) {
				loadData();
				loadPrihodki();
				loadOdhodki();
			}
			// pieCharts

		}

		var drawPieChart = function(transakcije, tortniDiagram) {

		}

		$scope.prihodkiTortniDiagram = {};

		// nastavi datume v url
		var changeDatesInUrl = function() {
			$location.search($scope.$strings.datumOd, $scope.startDate.date);
			$location.search($scope.$strings.datumDo, $scope.endDate.date);
		};

		$scope.setStartEndDate($scope.startDate, $scope.endDate, $stateParams,
				function(startDate, endDate) {
					$scope.startDate = startDate;
					$scope.endDate = endDate;
					changeDatesInUrl();
				});

		$scope.$watch('startDate.date', dateChange);
		$scope.$watch('endDate.date', dateChange);

		$scope.podrobnostiPrihodki = {
			prikazi : false
		}

		$scope.podrobnostiOdhodki = {
			prikazi : false
		}

		$scope.$watch('podrobnostiPrihodki.prikazi', function() {
			if ($scope.podrobnostiPrihodki.prikazi == true) {
				$state.go('^.transakcija', {
					transakcijaId : $scope.podrobnostiPrihodki.podrobnosti.id
				});
			}
		})

		$scope.$watch('podrobnostiOdhodki.prikazi', function() {
			if ($scope.podrobnostiOdhodki.prikazi == true) {
				$state.go('^.transakcija', {
					transakcijaId : $scope.podrobnostiOdhodki.podrobnosti.id
				});
			}
		})

		$scope.prihodkiData = [];
		$scope.odhodkiData = [];

		$scope.responsePrihodki = null;
		$scope.responseOdhodki = null;

		$scope.$watch("responsePrihodki.delezi.length", function() {
			if ($scope.responsePrihodki == undefined)
				return;
			var chartData = $scope.obdelajPrihodkeOdhodkePieChart(
					$scope.responsePrihodki, $scope.$strings.baseBarva1,
					$scope.$strings.baseBarva2);
			$scope.prihodkiData = chartData;
		});

		$scope.$watch("responseOdhodki.delezi.length", function() {
			if ($scope.responsePrihodki == undefined)
				return;
			var chartData = $scope.obdelajPrihodkeOdhodkePieChart(
					$scope.responseOdhodki, $scope.$strings.baseBarva2,
					$scope.$strings.baseBarva1);
			$scope.odhodkiData = chartData;
		});

		var loadPrihodki = function() {
			$scope.responsePrihodki = porocilaStrukturaService.prihodki.get({
				odDatum : $scope.startDate.date.toISOString(),
				doDatum : $scope.endDate.date.toISOString()
			});
		}

		var loadOdhodki = function() {
			$scope.responseOdhodki = porocilaStrukturaService.odhodki.get({
				odDatum : $scope.startDate.date.toISOString(),
				doDatum : $scope.endDate.date.toISOString()
			});
		}
		// select test
		$scope.vrstaPrometa = "ves";

		$scope.$watch("vrstaPrometa", function() {
			if (!$scope.isFirstLoad) {
				loadData();
			}
		});

		$scope.zamenjajPrikaz = function(tip) {
			$scope.prihodki = [];
			$scope.odhodki = [];

			$scope.sort = {
				sortirajPo : "naziv",
				ascOrDesc : "ASC"
			};

			$scope.prikazi = tip;

			if (!$scope.isFirstLoad) {
				loadData();
			}
		}

		// paginacija & search

		$scope.sort = {
			sortirajPo : "naziv",
			ascOrDesc : "ASC"
		};
		$scope.searchString = "";

		$scope.itemsPerPage = 8;
		$scope.maxSize = 5;
		$scope.currentPage = 1;

		var loadData = function() {
			$scope.isLoading = true;
			// najprej izvemo stevilo
			services[$scope.prikazi].stevilo.get({
				odDatum : $scope.startDate.date.toISOString(),
				doDatum : $scope.endDate.date.toISOString(),
				iskalniNiz : $scope.searchString,
				vrstaPrometa : $scope.vrstaPrometa
			}, function(stevilo) {
				$scope.totalItems = stevilo.stevilo;
				$scope.currentPage = 1;
				getTransakcije(0);
			});
		}
		
		loadData();
		loadPrihodki();
		loadOdhodki();

		$scope.loadData = loadData;
		
		var getTransakcije = function(offset) {
			services[$scope.prikazi].transakcije.query({
				odDatum : $scope.startDate.date.toISOString(),
				doDatum : $scope.endDate.date.toISOString(),
				stevilo : $scope.itemsPerPage,
				odmik : offset,
				sortiranjePo : $scope.sort.sortirajPo,
				vrstniRed : $scope.sort.ascOrDesc,
				iskalniNiz : $scope.searchString,
				vrstaPrometa : $scope.vrstaPrometa
			}, function(transakcije) {
				$scope.isLoading = false;
				$scope.isFirstLoad = false;
				if ($scope.prikazi == 0) {
					$scope.prihodki = transakcije;
				} else if ($scope.prikazi == 1) {
					$scope.odhodki = transakcije;
				}
			});
		}

		$scope.pageChanged = function() {
			getTransakcije(($scope.currentPage - 1) * $scope.itemsPerPage);
		}

		$scope.search = function() {
			loadData();
		};

		$scope.refresh = function() {
			$scope.searchString = "";
			loadData();
		};

	};
});