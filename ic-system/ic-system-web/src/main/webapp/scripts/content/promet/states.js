﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija podstrani zemljevid.
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './prometController', 'text!./promet.html'
], function ( strings, prometController, prometTemplate ) {
  return {
    id: 'promet',
    url: strings.promet.url,
    strings: strings.promet,
    template: prometTemplate,
    controller: prometController,
    isSubPage: true,
    icon: 'fa-automobile',
    reloadOnSearch: false
  };
} );
