﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran namizje (namizje.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/dogodek', 'lodash'
], function ( strings, _ ) {

  return /*@ngInject*/ function ( $scope, $stateParams, $state, dogodkiService) {
  	  		
    $scope.$strings = _.merge( {}, strings, $scope.$strings );
	
	$scope.curDogodek = dogodkiService.dogodki.get({
		dogodekId : $stateParams.dogodekId
	}, function(){
		if($scope.curDogodek.tip != 'drugo'){
			if( $scope.curDogodek.racun.vrsta ==  'izdani'){
				$scope.curDogodek.stranka = $scope.curDogodek.racun.prejemnikRacuna;
			}else if($scope.curDogodek.racun.vrsta == 'prejeti'){
				$scope.curDogodek.stranka = $scope.curDogodek.racun.izdajateljRacuna;
			}
		}
	});
	
	$scope.skrijPodrobnosti = function() {
		$state.go('^.domov');
	}

    
  };
} );