﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran namizje (namizje.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/domov', 'lodash', 'charts'
], function ( strings, _ , Chart) {

  return /*@ngInject*/ function ( $scope, $location, $modal, $log, $state, dogodkiService, prometService, porocilaPrometPregledService, googleService) {
  	  		
    $scope.$strings = _.merge( {}, strings, $scope.$strings );
	var pregledChart;
	var pregledChartCtx;
	var chartJsOptions = {
		scaleFontSize : 12
	}
	var today = new Date();
    $scope.endDate = {
    	date: today
    }
	
    $scope.dovoljPodatkovZaGraf = false;
    
	var startDate = new Date();
	startDate.setMonth(startDate.getMonth() - 12);
    $scope.startDate = {
    	date: startDate
    }
  	
	$scope.responsePregled = null;
	
	var successPregled = function() {
		if($scope.responsePregled == undefined ) return;
    
		if($scope.responsePregled.length > 1) {
    		$scope.dovoljPodatkovZaGraf = true;
	    	var dataGraph = $scope.obdelajIzdaniPrejetiLineChart($scope.responsePregled);
	    	var chartDataContainer = {
	    			dataGraph: dataGraph,
	    			canvasId: '#pregledChart',
	    			canvasContainerId: '#chartContainer',
	    			chartCtx: pregledChartCtx,
	    			chart: pregledChart
	    	}
	    	
	    	chartDataContainer = $scope.drawLineGraph(chartDataContainer);
	    	pregledChartCtx = chartDataContainer.chartCtx;
	    	pregledChart = chartDataContainer.chart;

		} else {
    		$scope.dovoljPodatkovZaGraf = false;
		}
    }
	
    $scope.$watch("responsePregled.length", successPregled);
	
  	var loadPregled = function() {
		 $scope.responsePregled = porocilaPrometPregledService.pregled.query(
				{odDatum: $scope.startDate.date.toISOString() , doDatum: $scope.endDate.date.toISOString() });  
  	}
  	
  	
	$scope.$watch('prometChartChoice', function(newValue, oldValue) {
		
		var startDate = new Date();
		startDate.setMonth(startDate.getMonth() - newValue)
		$scope.startDate = {
			date: startDate
		}

		//narisi graf na podlagi $scope.startDate in $scope.endDate
		loadPregled();
		
	});
	
	//preusmeri na drugo stran
  	$scope.go = function ( view ) {
  		$state.go( view );
	};	
	
	//podrobnosti dogodka
	$scope.openDogodek = function (dogodek){
		$state.go('^.dogodek', {dogodekId: dogodek.id});
	}

	$scope.prometChartChoice = 12;

	//REST

	prometService.stanje.get({}, function(stanje){
		$scope.stanje = stanje.stanje;
  	});

  	dogodkiService.nasvet.get({}, function(nasvet){
  		$scope.nasvetRacunovodje = nasvet.opis;
  	});
	
  	//paginacija 

  	//najprej moramo izvedeti stevilo dogodkov
  	dogodkiService.dogodkiStevilo.get({}, function(stevilo){
  		$scope.totalItems = stevilo.stevilo;
  		$scope.currentPage = 1;
  		$scope.itemsPerPage = 6;
  		//nato naloadamo ustrezno stevilo dogodkov
  		dogodkiService.dogodki.query({
  			stevilo: $scope.itemsPerPage,
  			odmik: 0
  		}, function(dogodki){
  			$scope.dogodki = dogodki;
  		});

  	});

  	$scope.pageChanged = function(){

  		dogodkiService.dogodki.query({
  			stevilo: $scope.itemsPerPage,
  			odmik: ($scope.currentPage - 1) * $scope.itemsPerPage 
  		}, function(dogodki){
  			$scope.dogodki = dogodki;
  		});

  		console.log($scope.currentPage);
  	}

  	// google code response
  	var queryParams = $location.search();
  	if (queryParams.code != undefined) {
  		// send auth code to backend
  		googleService.googleConnect.insert({
  			code: queryParams.code
  		}, function() {
        $scope.getGoogleRacun();
  			console.log('sent google code: ' + queryParams.code);
  		});
  	}

  };
} );