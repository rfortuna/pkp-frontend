﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija stanja aplikacije (id: 'namizje')
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './dogodekController', 'text!./dogodek.html'
], function ( strings, dogodekController, dogodekTemplate) {
  return {
    id: 'dogodek',
    url: strings.dogodek.url,
    strings: strings.dogodek,
    template: dogodekTemplate,
    controller: dogodekController,
    isSubPage: true,
    icon: 'fa-home',	
    requiresAuth: true,
    hasParams: true
  };
} );
