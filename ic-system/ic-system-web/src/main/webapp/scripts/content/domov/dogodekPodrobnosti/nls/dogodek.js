/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Nizi za podstran namizje.
 *
 * @ngdoc object
 */
define({
  root: {
  
    dogodekSkrijPodrobnosti: 'Skrij podrobnosti',

    dogodekTitle: 'Dogodek',
    dogodekDatumIzdaje: 'Datum izdaje',
    dogodekRokPlacila: 'Rok placila',
    dogodekZnesek : 'Znesek',
    dogodekOpis : 'Opis',
    dogodekStatus: 'Status',
    dogodekKontaktnaOseba: 'Kontaktna oseba',
    dogodekBtnEmail : 'Pošlji email',
    dogodekClose: 'Close',
    dogodekVrsta: 'Vrsta',
    dogodekNaslov: 'Naslov',
    dogodekIdddv: 'ID-DDV'

  }
} );
