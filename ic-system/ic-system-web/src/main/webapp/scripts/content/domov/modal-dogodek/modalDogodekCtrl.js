/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Direktiva za prikaz jarviswidgeta.
 *
 * @ngdoc directive
 */
define(['i18n!../nls/domov' , 'lodash'],  function (strings, _) {
	return /*@ngInject*/ function ( $scope, $modalInstance, dogodek ) {

		$scope.$strings = _.merge( {}, strings, $scope.$strings );

		$scope.dogodek = dogodek;
		$scope.close = function () {
			$modalInstance.dismiss();
		};
		$scope.sendEmail = function(email) {
		    var link = "mailto:"+ email;
		    window.location.href = link;
		};
	}
});