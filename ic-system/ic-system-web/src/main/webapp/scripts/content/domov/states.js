﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija stanja aplikacije (id: 'namizje')
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './domovController', 'text!./domov.html'
], function ( strings, domovController, domovTemplate) {
  return {
    id: 'domov',
    url: strings.domov.url,
    strings: strings.domov,
    template: domovTemplate,
    controller: domovController,
    isSubPage: true,
    icon: 'fa-home',	
    requiresAuth: true
  };
} );
