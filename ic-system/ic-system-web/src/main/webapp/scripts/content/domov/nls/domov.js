/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Nizi za podstran namizje.
 *
 * @ngdoc object
 */
define({
  root: {
    detailsIcon: 'fa-question',

    headerDogodki: 'Dogodki',
    headerPromet: 'Promet',
    headerNasvet: 'Nasvet računovodje',
    selectChart : {
    	selectText: 'Prikaz prometa za: ',
    	choices : [
    	           {text: 'zadnjih 12 mesecev', value:12},
    	           {text: 'zadnjih 6 mesecev',value:6},
    	           {text: 'zadnjih 3 mesecev', value:3}
    	           ]
    },

    labelStanje: 'Stanje na računu',

    btnPregledStanja: 'Pregled stanja',
    btnPrihodki: 'Prihodki',
    btnOdhodki: 'Odhodki', 

    dogodekSkrijPodrobnosti: 'Skrij podrobnosti',

    dogodekTitle: 'Dogodek',
    dogodekDatumIzdaje: 'Datum izdaje',
    dogodekRokPlacila: 'Rok placila',
    dogodekZnesek : 'Znesek',
    dogodekOpis : 'Opis',
    dogodekStatus: 'Status',
    dogodekKontaktnaOseba: 'Kontaktna oseba',
    dogodekBtnEmail : 'Pošlji email',
    dogodekClose: 'Close',
    dogodekVrsta: 'Vrsta',
    dogodekNaslov: 'Naslov',
    dogodekIdddv: 'ID-DDV'

  }
} );
