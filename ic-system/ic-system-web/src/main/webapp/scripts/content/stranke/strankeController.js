﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran namizje (namizje.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/stranke', 'lodash'
], function ( strings, _ ) {

  return /*@ngInject*/ function ( $scope, $stateParams, $location, strankeService ) {

    $scope.$strings = _.merge( {}, strings, $scope.$strings );

    $scope.stranke = [];
    
    $scope.activestranka = {};

    var services = [strankeService.abecedni, strankeService.stalne, strankeService.dolzniki];

    $scope.service = services[0];

    
    $scope.searchString ="";

    //iskanje strank
    $scope.search = function(){
        loadData();
    };

    //podrobnosti stranke
    var setActiveStranka = function(stranka){
        strankeService.stranka.get({
          id: stranka.id
        }, function(stranka){
          $scope.activestranka = stranka;
        });
    }
    $scope.setActiveStranka = setActiveStranka;

    //nastavitve paginacije
    $scope.itemsPerPage = 8;
    $scope.maxSize = 3;
    $scope.isFirstLoad = true;
    
    $scope.tabChanged = function(serviceIndex){
    	if(!$scope.isFirstLoad){
	        $scope.searchString = "";
	    	$scope.service = services[serviceIndex];
	        loadData();
    	}
    };

    
    //funkcija za loadanje strank
    var loadData = function(){
        $scope.service.stevilo.get({
            iskalniNiz: $scope.searchString
        }, function(stevilo){
            $scope.totalItems = stevilo.stevilo;
            $scope.currentPage = 1;
            //nato naloadamo ustrezno stevilo dogodkov
            $scope.service.stranke.query({
                stevilo: $scope.itemsPerPage,
                odmik: 0,
                iskalniNiz: $scope.searchString
            }, function(stranke){
                $scope.isFirstLoad = false;
                $scope.stranke = stranke;
                if(stranke.length > 0){
                    setActiveStranka(stranke[0]);
                }
            });
        });
    }
    
    loadData();
      

    //paginacija
    $scope.pageChanged = function(){
      $scope.service.stranke.query({
        stevilo: $scope.itemsPerPage,
        odmik: ($scope.currentPage - 1) * $scope.itemsPerPage,
        iskalniNiz: $scope.searchString
      }, function(stranke){
        $scope.stranke = stranke;
      });
    }

  };
} );