﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran namizje (namizje.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/podrobnosti', 'lodash', 'jQuery'
], function ( strings, _ , $) {

  return /*@ngInject*/ function ( $scope, $stateParams, $state, $location, strankeService, sifrantiService) {
  	  		
  $scope.$strings = _.merge( {}, strings, $scope.$strings );
	
	var prometChart, prometChartCtx;

  $scope.dovoljPodatkovZaGraf = false;
  
  
  $scope.canEdit = $.inArray('admin', $scope.keycloak.realmAccess.roles) > -1;
  //TODO odkomentiraj scene za MAPO
	    
    //nastavi datume v url
	var changeDatesInUrl = function() {
		$location.search($scope.$strings.datumOd, $scope.startDate.date);
		$location.search($scope.$strings.datumDo, $scope.endDate.date);
	};
    
    //nastavi datume na zacetku
	$scope.setStartEndDate($scope.startDate, $scope.endDate, $stateParams, 
		function(startDate, endDate) {
			$scope.startDate = startDate;
			$scope.endDate = endDate;
			changeDatesInUrl();
	});
	
	function dateChange(){
      changeDatesInUrl();
      if(typeof $scope.activestranka.id != 'undefined' ){
        loadData();
        loadGraphData();
      } 
    }
	
	$scope.$watch('startDate.date', dateChange);
	$scope.$watch('endDate.date', dateChange);
	    
    
	$scope.activestranka = strankeService.stranka.get({
			id : $stateParams.strankaId
		}, function(){
	     changeMapPosition();
		 loadData();
	     loadGraphData();
	});
	
	
	$scope.skrijPodrobnosti = function() {
		$state.go('^.stranke');
	}
	
	$scope.responseStrankaGraph = [];
	    
    	$scope.$watch('responseStrankaGraph.length', function() {
    	if($scope.responseStrankaGraph == undefined ) return;
    	
    	if($scope.responseStrankaGraph.length > 1) {
    		
    		$scope.dovoljPodatkovZaGraf = true;
    		setTimeout(function(){
    			var dataGraph = $scope.obdelajIzdaniPrejetiLineChart($scope.responseStrankaGraph);
    	    	
    	    	var chartDataContainer = {
    	    			dataGraph: dataGraph,
    	    			canvasId: '#strankaPrometChart',
    	    			canvasContainerId: '#strankaPrometChartContainer',
    	    			chartCtx: prometChartCtx,
    	    			chart: prometChart
    	    	}
    	    	
    	    	chartDataContainer = $scope.drawLineGraph(chartDataContainer);
    	    	prometChartCtx = chartDataContainer.chartCtx;
    	    	prometChart = chartDataContainer.chart;
    		}, 100);
		} else {
    		$scope.dovoljPodatkovZaGraf = false;
		}

    });
    
    var loadGraphData = function () {
    	if($scope.activestranka == undefined) return;
    	$scope.responseStrankaGraph = strankeService.strankaGraf.query(
    			{id: $scope.activestranka.id, odDatum: $scope.startDate.date.toISOString() , doDatum: $scope.endDate.date.toISOString() });  
    }

    //paginacija & search

    $scope.sort = {
        sortirajPo : "sn",
        ascOrDesc: "ASC"
    };

    $scope.searchString = "";
    

    $scope.itemsPerPage = 8;
    $scope.maxSize = 5;
    $scope.isFirstLoad = true;

    var loadData = function(){
        strankeService.strankaTransakcije.stevilo.get({
            id: $stateParams.strankaId,
            odDatum: $scope.startDate.date.toISOString(),
            doDatum: $scope.endDate.date.toISOString(),
            iskalniNiz: $scope.searchString
           //search param
         }, function(stevilo){
            $scope.totalItems = stevilo.stevilo;
            $scope.currentPage = 1;
            getTransakcije(0); 
         } 
        );
    }
    loadData();

    $scope.loadData = loadData;

    var getTransakcije = function(offset){
        strankeService.strankaTransakcije.transakcije.query({
            id: $stateParams.strankaId,
            odDatum: $scope.startDate.date.toISOString(),
            doDatum: $scope.endDate.date.toISOString(),
            stevilo: $scope.itemsPerPage,
            odmik: offset,
            sortiranjePo: $scope.sort.sortirajPo,
            vrstniRed: $scope.sort.ascOrDesc,
            iskalniNiz: $scope.searchString
        }, function(transakcije){
            $scope.isFirstLoad = false;
            $scope.activestranka.transakcije = transakcije;
        });
    }

    $scope.pageChanged = function(){
        getTransakcije( ($scope.currentPage - 1) * $scope.itemsPerPage );
    }

    $scope.search = function(){
      loadData();
    };

    $scope.refresh = function(){
      $scope.searchString = "";
      loadData();
    };
    
    
    $scope.podrobnostiTransakcije = {
    		prikazi : false,
    		podrobnosti : {}
    };
    
    $scope.$watch("podrobnostiTransakcije.prikazi", function(){
      if($scope.podrobnostiTransakcije.prikazi != false){
	      $state.go("^.transakcijaStranka", {
	        transakcijaId: $scope.podrobnostiTransakcije.podrobnosti.id,
	        strankaId: $stateParams.strankaId
	      });
      }
    });
    
    //maps
    
    $scope.map = { show: true ,center: { latitude: 13, longitude: 15 }, zoom: 15, pan: true };
    $scope.marker = { center: { latitude: 16, longitude: 14 }, id: "one" };
    
    var changeMapPosition = function() {
    	if($scope.activestranka.naslov.latitude == 0 || $scope.activestranka.naslov.longitude == 0) {
    		$scope.map.show = false;
    	} else {
    	  $scope.map.show = true;
	      $scope.map.center.latitude = $scope.activestranka.naslov.latitude;
	      $scope.map.center.longitude = $scope.activestranka.naslov.longitude;
	      $scope.marker.center.latitude = $scope.activestranka.naslov.latitude;
	      $scope.marker.center.longitude = $scope.activestranka.naslov.longitude;
    	}
    } 
    
    
    //editiranje naslova stranke
    $scope.editMode = false;
    $scope.oldactivestranka = {};
    $scope.saveError = false;
    $scope.saveErrorMessage = "";
    
    $scope.openEditMode = function(){
		$scope.editMode = true;
		angular.copy($scope.activestranka, $scope.oldactivestranka);
		
		//pridobimo seznam vseh mest glede na drzavo stranke
		getMesta($scope.activestranka.naslov.drzava.id, false);
    };
    
    $scope.closeEditMode = function(){    		
    	$scope.editMode =false;
    	$scope.saveError = false;
    	angular.copy($scope.oldactivestranka, $scope.activestranka);
    };
    
    $scope.saveNaslov = function(){

    	var geocoder = new google.maps.Geocoder();
    	geocoder.geocode( { 
    		'address': $scope.activestranka.naslov.ulica +
			" " + $scope.activestranka.naslov.hisnaStevilka+
			" " + $scope.activestranka.naslov.posta.mesto + 
			" " + $scope.activestranka.naslov.drzava.ime
    	}, function(results, status) {
    		if(status == google.maps.GeocoderStatus.ZERO_RESULTS){
    			$scope.saveError = true;
    		}
    		else if(status == google.maps.GeocoderStatus.OK){
    			
    			var lat = results[0].geometry.location.k;
    			var long = results[0].geometry.location.D;
    			strankeService.strankaLokacija.update({
    	    		strankaId: $scope.activestranka.id,
    	    		drzavaId: $scope.activestranka.naslov.drzava.id,
    	    		hisnaSt: $scope.activestranka.naslov.hisnaStevilka,
    	    		ulica: $scope.activestranka.naslov.ulica,
    	    		postnaSt: $scope.activestranka.naslov.posta.postnaSt.toString(),
    	    		latitude: lat,
    	    		longitude: long
    	    	
    	    	}, function(){
    	    		
        			$scope.map.center.latitude = lat;
            		$scope.map.center.longitude = long;
            		$scope.marker.center.latitude = lat;
            		$scope.marker.center.longitude = long;
            		$scope.map.show = true;
            		//$scope.activestranka.naslov.mesto = 
    	    		
    	    		$scope.saveError = false;
    	    		$scope.editMode = false;
    	    	}, function(error){
    	    		$scope.saveError = true;
    	    	});
    			
    		}else{
    			$scope.saveError = false;
    			//$scope.saveErrorMessage = "Pri iskanju geolokacije je prišlo do napake. Poskusite vnesti veljavno lokacijo."
    		}
    	});
    };
    
	//dropdowni za drzave in mesta
    
    var getDrzave = function (){
    	sifrantiService.drzave.query({}, function(drzave){
    		$scope.vseDrzave = drzave;
    	});
    }
    getDrzave();
    
    
    var getMesta = function(idDrzave, firstTime){
    	sifrantiService.mesta.query({
    		id: idDrzave
    	},function(mesta){
    		$scope.vsaMesta = mesta;
    		
    		if(firstTime){
    			$scope.activestranka.naslov.posta = $scope.vsaMesta[0];
    		}else{
    			angular.forEach($scope.vsaMesta, function(mesto){
    				if(mesto.postnaSt == $scope.activestranka.naslov.posta.postnaSt){
    					$scope.activestranka.naslov.posta = mesto;
    				}
    			});
    		}
    	});
    }
	
	$scope.changeDrzava = function (){
		getMesta($scope.activestranka.naslov.drzava.id, true);
	};
    
  };
} );