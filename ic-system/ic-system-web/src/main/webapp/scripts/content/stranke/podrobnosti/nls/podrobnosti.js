define({
  root: {
  	headerSeznamStrank :'Seznam strank',
  	headerStranka: 'Stranka',
  	naslov: 'Naslov',
  	kontaktnaOseba: 'Kontaktna oseba',
  	idddv :'ID-DDV',
  	obdobjeOd: 'Obdobje od',
    obdobjeDo: 'do',
    datumOd: "datumOd",
  	datumDo: "datumDo",
  	opozoriloPremaloPodatkovZaGraf: "Na voljo je premalo podatkov za izris grafa!",
  	strankaSkrijPodrobnosti: "Skrij podrobnosti",
    nasloviTabele: [ 
              {
                naslov: 'sn',
                sort:"sn"
              },
              { 
              naslov: 'Vrsta',
              sort: "vrsta"
              },
              {
                naslov: 'Datum',
                sort: "datumIzdaje"
              }
              , 
              {
                naslov: 'Znesek',
                sort:"znesek"
              }],
    iskanje: 'Iskanje po stroškovnih nosilcih:',
    alertNiRezultatov: 'V bazi ni podatkov ki ustrezajo vašemu iskanju.',
    zacniUrejanje: 'Uredi naslov',
    prenehajUrejanje: 'Prenehaj z urejanjem',
    shraniNaslov: 'Shrani naslov',
    editModeUlicaHisna: 'Ulica in hišna št.:',
    editModePostaMesto: 'Poštna št. in mesto:',
    editModeDrzava: 'Država:'
    
    	
  }
} );
