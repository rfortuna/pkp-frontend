/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Nizi za podstran namizje.
 *
 * @ngdoc object
 */
define({
  root: {
  	headerSeznamStrank :'Seznam strank',
  	headerStranka: 'Stranka',
  	naslov: 'Naslov',
  	kontaktnaOseba: 'Kontaktna oseba',
  	idddv :'ID-DDV',
  	obdobjeOd: 'Obdobje od',
    obdobjeDo: 'do',
    tabHeaderVse: 'Vse',
    tabHeaderStalne: 'Stalne',
    tabHeaderDolzniki: 'Dolžniki',
    alertNiRezultatov: 'Iskanje ni dalo rezultatov.',
    iskanje: 'Iskanje'
    
  }
} );
