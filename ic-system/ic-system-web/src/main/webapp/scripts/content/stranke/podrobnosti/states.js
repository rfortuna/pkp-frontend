﻿
define([
  'i18n!./nls/states', './podrobnostiController', 'text!./podrobnosti.html'
], function ( strings, podrobnostiController, podrobnostiTemplate) {
  return {
    id: 'stranka',
    url: strings.podrobnosti.url,
    strings: strings.podrobnosti,
    template: podrobnostiTemplate,
    controller: podrobnostiController,
    isSubPage: true,
    icon: 'fa-home',	
    requiresAuth: true,
    hasParams: true,
    reloadOnSearch: false
  };
} );
