﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Metapodatki za podstran namizje.
 *
 * @ngdoc object
 */
define({
  root: {
    podrobnosti: {
      url: '/transakcijaStranka/{transakcijaId}?strankaId',
      title: 'Podrobnosti transakcije',
      longTitle: 'Podrobnosti transakcije',
      description: 'Podrobnosti transakcije',
      keywords: 'transakcija'
    }
  }
} );
