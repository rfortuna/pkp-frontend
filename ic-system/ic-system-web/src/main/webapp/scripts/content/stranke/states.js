﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija stanja aplikacije (id: 'namizje')
 *
 * @ngdoc object
 */
define([

  'i18n!./nls/states', './strankeController', 'text!./stranke.html'
], function ( strings, strankeController, strankeTemplate) {
  return {
    id: 'stranke',
    url: strings.stranke.url,
    strings: strings.stranke,
    template: strankeTemplate,
    controller: strankeController,
    isSubPage: true,
    icon: 'fa-users'
  };
} );
