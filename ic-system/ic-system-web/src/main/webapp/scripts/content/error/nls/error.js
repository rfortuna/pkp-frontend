/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Nizi za podstran namizje.
 *
 * @ngdoc object
 */
define({
  root: {
    errors : {
        403 : 'Nimate pravic za dostop do zahteve strani.',
        404 : 'Zahtevana stran ni bila najdena.',
        401 : 'Uporabnik ni prijavljen.',
        0 : 'No status code.'
    }
  }
} );
