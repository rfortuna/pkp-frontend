﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija stanja aplikacije (id: 'namizje')
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './errorController', 'text!./error.html'
], function ( strings, errorController, errorTemplate) {
  return {
    id: 'error',
    url: strings.error.url,
    strings: strings.error,
    template: errorTemplate,
    controller: errorController,
    isSubPage: true,
    hasParams: true,
    icon: 'fa-frown-o'
  };
} );
