/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za ogrodje spletne aplikacije (layout.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/layout', 'lodash', 'jQuery-ui', 'bootstrap', 'smart-notification', 'smart-widgets'
], function ( strings, _ ) {

  return /*@ngInject*/ function ( $scope, $state, $stateParams, localStorageService) {

    $scope.$strings = _.merge( {}, strings, $scope.$strings );

    $scope.getStatePath = function () {
      var currentState = $scope.$state.current;
      var depth = currentState.depth;

      var statePath = [ currentState ];
      for ( i = 0; i < depth; i++ ) {
        currentState = currentState.parentState;
        statePath.push( currentState );
      }
      return statePath.reverse();
    };

    $scope.isValidLink = function ( state ) {
      if ( !state.abstract && !state.hasParams && !state.visible &&
           !($scope.isMobileDevice() && state.children[ 0 ].requiresAuth) ) return true;
      else return !!(state.abstract && state.children && !state.hasParams && state.children[ 0 ] &&
                     !state.children[ 0 ].abstract && !state.children[ 0 ].hasParams && state.children[ 0 ].visible &&
                     !($scope.isMobileDevice() && state.children[ 0 ].requiresAuth));
    };

    $scope.goToState = function ( state ) {
      if ( !state.abstract && !state.hasParams && !state.visible &&
           !($scope.isMobileDevice() && state.children[ 0 ].requiresAuth) ) $state.transitionTo( state.absoluteId );
      else if ( state.abstract && state.children && !state.hasParams && state.children[ 0 ] &&
                !state.children[ 0 ].abstract && !state.children[ 0 ].hasParams && state.children[ 0 ].visible &&
                !($scope.isMobileDevice() && state.children[ 0 ].requiresAuth) )
        $state.transitionTo( state.children[ 0 ].absoluteId );
    };

    $scope.login = function () {

    };

    $scope.logout = function () {
    	if(localStorageService.isSupported) {
			localStorageService.remove("lastTime");
			localStorageService.remove("lastLatitude");
			localStorageService.remove("lastLongitude");
		}
    	$scope.keycloak.logout({redirectUri: 'http://localhost:8080/ic-system-web/#!/domov'});
    };

    $scope.isMobileDevice = function () {
      return $( 'body' ).hasClass( 'mobile-detected' ) ? true : false;
    }
    
    $scope.shadeColor = function (color, percent) {  
        var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
        return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (G<255?G<1?0:G:255)*0x100 + (B<255?B<1?0:B:255)).toString(16).slice(1);
    }
  };
} );