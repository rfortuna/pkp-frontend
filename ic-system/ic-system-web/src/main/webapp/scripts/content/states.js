﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija ogrodja aplikacije.
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './layoutController', 'text!./layout.html', './domov/states', 
  './promet/states', './stranke/states', './porocila/states', './domov/dogodekPodrobnosti/states', './stranke/podrobnosti/states',
  './promet/podrobnosti/states', './stranke/transakcijePodrobnosti/podrobnosti/states', './error/states', './location/states'
], function ( strings, layoutController, layoutTemplate, domovStates, 
		prometStates, strankeStates, porocilaStates, dogodekStates, strankePodrobnostiStates,
		prometPodrobnostiStates, transakcijePodrobnostiStates, errorStates, locationStates) {
  return {
    defaultPath: 'domov.domov',
    id: 'domov',
    url: '',
    abstract: true,
    visible: false,
    isSubPage: false,
    strings: strings.layout,
    template: layoutTemplate,
    controller: layoutController,
    children: [ domovStates, prometStates, strankeStates, porocilaStates, dogodekStates, strankePodrobnostiStates, prometPodrobnostiStates, transakcijePodrobnostiStates, errorStates, locationStates]
  };
} );