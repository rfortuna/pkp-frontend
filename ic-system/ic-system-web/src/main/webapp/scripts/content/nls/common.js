﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Pogosti uporabljeni nizi spletne aplikacije.
 *
 * @ngdoc object
 */
define({
  root: {

    copyright: "Copyright © 2014. Vse pravice pridržane.",
    
    pieChartProcent: 0.10,
    pieChartKorakOsvetlitve: 10,
    baseBarva1: "#a5e9ff",
	baseBarva2: "#ffe666",
	opozoriloPremaloPodatkovZaGraf: "Na voljo je premalo podatkov za izris grafa!",
	datumOd: "datumOd",
	datumDo: "datumDo",
    errors: {
      noRoleTitle: 'Ni dovoljeno',
      noRoleText: 'Nimate zadostnih pravic za dostop.',
      unknownTitle: 'Error',
      unknownText: 'There was an error. Please try again.',

      organaNiTitle: 'Organ ne obstaja',
      organaNiText: 'Zahtevanega organa ni bilo mogoče najdti. Prosimo ponovno preverite seznam organov',

      required: 'This field is required',
      email: 'Please enter a valid email',
      minLength: 'Minimum number of letters: ',
      maxLength: 'Maximum number of letters: ',
      number: 'Please enter a number',
      url: 'The URL must be specified in the following format: \'http://domain/uri\''
    }
  }
} );