﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Nizi za ogrodje aplikacije.
 *
 * @ngdoc object
 */
define({
  root: {
    collapse_menu: 'Close menu',
    full_screen: 'Full screen',
    sign_out: 'Log out',
    sign_in: 'Log in',
    search_button: 'Search',
    search_placeholder: 'Search...',
    search_cancel: 'Cancel search',
    me: 'Me'
  }
});
