/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Metpodatki za ogrodje aplikacije.
 *
 * @ngdoc object
 */
define({
  root: {
    layout: {
      url: '',
      title: 'Home',
      longTitle: 'Home',
      description: 'The base framework of the application',
      keywords: 'framework'
    }
  }
});
