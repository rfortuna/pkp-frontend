﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran namizje (namizje.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/promet', 'lodash'
], function ( strings, _ ) {

  return /*@ngInject*/ function ( $scope ) {

    $scope.$strings = _.merge( {}, strings, $scope.$strings );
    
  };
} );