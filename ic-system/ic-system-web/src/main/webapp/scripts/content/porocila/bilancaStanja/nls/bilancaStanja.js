/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Nizi za podstran namizje.
 *
 * @ngdoc object
 */
define({
  root: {
	  textKategorije: "Kategorije",
	  textKategorijaOpis: "Opis",
	  textKategorijaGraf: "Primerjava 2013 in 2014",
	  textNiPodatkov: {
		   "kategorija": "Podatki trenutno zal niso na voljo",
		   "opis":"Podatki trenutno zal niso na voljo"
	  }
		  
  }
} );
