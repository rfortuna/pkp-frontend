﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija stanja aplikacije (id: 'namizje')
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './bilancaStanjaController', 'text!./bilancaStanja.html'
], function ( strings, bilancaStanjaController, bilancaStanjaTemplate ) {
  return {
    id: 'bilancaStanja',
    url: strings.bilancaStanja.url,
    strings: strings.bilancaStanja,
    template: bilancaStanjaTemplate,
    controller: bilancaStanjaController,
    isSubPage: true,
    icon: 'fa-list',
    requiresAuth: true,
    requiredRoles: ['direktor']
  };
} );
