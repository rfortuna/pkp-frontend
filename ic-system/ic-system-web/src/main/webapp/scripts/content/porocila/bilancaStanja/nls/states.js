﻿define({
  root: {
    bilancaStanja: {
      url: '/bilancaStanja',
      title: 'Bilanca stanja',
      longTitle: 'Bilanca',
      description: 'Pregled transakcij.',
      keywords: 'pregled, transakcije'
    }
  }
} );
