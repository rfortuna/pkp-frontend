﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran namizje (namizje.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/pregled', 'lodash'
], function ( strings, _ ) {

  return /*@ngInject*/ function ( $scope , $stateParams, $location, porocilaPrometPregledService) {
	  
    $scope.$strings = _.merge( {}, strings, $scope.$strings );
    
    var pregledChartCtx, napovedPoslovanjaChartCtx;
	var pregledChart, napovedPoslovanjaChart;
	
	var firstLoad = true;
	
	$scope.responsePregled = null;
	$scope.responseNapoved = null;
	
	$scope.dovoljPodatkovZaGraf = false;
	
    var successPregled = function(newValue, oldValue) {
    	if($scope.responsePregled == undefined)  return; 
    	
    	if($scope.responsePregled.length > 1) {
    		$scope.dovoljPodatkovZaGraf = true;
    		setTimeout(function(){
		    	var dataGraph = $scope.obdelajIzdaniPrejetiLineChart($scope.responsePregled);
		    	var chartDataContainer = {
		    			dataGraph: dataGraph,
		    			canvasId: '#pregledChart',
		    			canvasContainerId: '#pregledChartContainer',
		    			chartCtx: pregledChartCtx,
		    			chart: pregledChart
		    	}
		    	
		    	chartDataContainer = $scope.drawLineGraph(chartDataContainer);
		    	pregledChartCtx = chartDataContainer.chartCtx;
		    	pregledChart = chartDataContainer.chart;
    		}, 100);
		} else {
    		$scope.dovoljPodatkovZaGraf = false;
		}

    }
    
    var obdelajPodatkeNapoved = function(responseData) {
    	
    	var labels = [];
    	var datasets = [{
	    		label: "Napoved",
	            fillColor: "rgba(220,220,220,0.2)",
	            strokeColor: "rgba(220,220,220,1)",
	            pointColor: "rgba(220,220,220,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(220,220,220,1)",
	            data: []}
    	];
    	
    	angular.forEach(responseData, function(obj){
    		labels.push(obj.obdobje);
    		datasets[0].data.push(obj.dnevniZnesek);
    	});
    
    	var dataGraph = { labels : labels, datasets: datasets};
    	
    	return dataGraph;
    	
    }
    
    var successNapoved = function() {
    	if($scope.responseNapoved == undefined) { return }
    	if($scope.responseNapoved.length > 1) {
    		$scope.dovoljPodatkovZaGraf = true;
    		setTimeout(function(){
		    	var dataGraph = obdelajPodatkeNapoved($scope.responseNapoved);
		    	var chartDataContainer = {
		    			dataGraph: dataGraph,
		    			canvasId: '#napovedPoslovanjaChart',
		    			canvasContainerId: '#napovedPoslovanjaChartContainer',
		    			chartCtx: napovedPoslovanjaChartCtx,
		    			chart: napovedPoslovanjaChart
		    	}
		    	
		    	chartDataContainer = $scope.drawLineGraph(chartDataContainer);
		    	napovedPoslovanjaChartCtx = chartDataContainer.chartCtx;
		    	napovedPoslovanjaChart = chartDataContainer.chart;
    		}, 100);
		} else {
    		$scope.dovoljPodatkovZaGraf = false;
		}
    	
    }
    
    
    
    $scope.$watch("responsePregled.length", successPregled);
    $scope.$watch("responseNapoved.length", successNapoved);
    
    var loadPregled = function() {
    	 $scope.responsePregled = porocilaPrometPregledService.pregled.query(
    			{odDatum: $scope.startDate.date.toISOString() , doDatum: $scope.endDate.date.toISOString() });  
    }
    
    var loadNapoved = function() {
    	$scope.responseNapoved = porocilaPrometPregledService.napoved.query(
    			{odDatum: $scope.startDate.date.toISOString() , doDatum: $scope.endDate.date.toISOString() });  
    }
    
    //nastavi datume v url
	var changeDatesInUrl = function() {
		$location.search($scope.$strings.datumOd, $scope.startDate.date);
		$location.search($scope.$strings.datumDo, $scope.endDate.date);
	};
    
    var dateChange = function(oldValue, newValue) {
    	if(firstLoad) {
    		firstLoad = false;
    		return;
    	}
    	loadPregled();
    	loadNapoved();
    	changeDatesInUrl();
    }

    //nastavi datume na zacetku
	$scope.setStartEndDate($scope.$startDate, $scope.$endDate, $stateParams, 
		function(startDate, endDate) {
			$scope.startDate = startDate;
			$scope.endDate = endDate;
			changeDatesInUrl();
	});
   
    
    $scope.$watch('startDate.date', dateChange);
    $scope.$watch('endDate.date', dateChange);
  
  };
} );