/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Nizi za podstran zemljevid.
 *
 * @ngdoc object
 */
define({
  root: {
	header: 'Poročila',
	icon: 'fa-bar-chart',
    content: 'Hello!',
    obdobjeOd: 'Obdobje od',
    obdobjeDo: 'do'
  }
});
