﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran namizje (namizje.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/izkazPoslovnegaIzida', 'lodash'
], function ( strings, _ ) {

  return /*@ngInject*/ function ( $scope, porocilaIzkazService ) {

	  
    $scope.$strings = _.merge( {}, strings, $scope.$strings );
    
    var selected = {};
    
    $scope.podatkiGrafa = {
    	labels : [],
    	datasets: []
    }
    $scope.kategorije = [];

    $scope.$watch('podrobnostiKategorije.zneski.length', function(){
    	if($scope.podrobnostiKategorije == undefined) return;
    	labels = [];
    	datasets = [{
			label: "Vrednost kategorije",
			 fillColor: "rgba(151,187,205,0.5)",
	         strokeColor: "rgba(151,187,205,0.8)",
	         highlightFill: "rgba(151,187,205,0.75)",
	         highlightStroke: "rgba(151,187,205,1)",
            data: []
    	}];
    	
    	angular.forEach($scope.podrobnostiKategorije.zneski, function(obj){
    		labels.push(obj.leto);
    		datasets[0].data.push(obj.znesek);
    	});
    	    	
    	$scope.podatkiGrafa = {
    			labels : labels,
    			datasets: datasets
    	}
    	
    });
        
    var getPodrobnosti = function(kategorija) {
    	$scope.podrobnostiKategorije = porocilaIzkazService.kategorije.get({
			kategorijaId : kategorija.id
		});
    }
    
    $scope.$watch('kategorije.length', function(){
    	if($scope.kategorije.length == 0) {
    		$scope.podrobnostiKategorije = $scope.$strings.textNiPodatkov;
    	} else {
    		getPodrobnosti($scope.kategorije[0]);
    	}
    });
    
    
    $scope.zamenjajPodrobnosti = function(kategorija) {
    	getPodrobnosti(kategorija);
        kategorija.isSelected = true;
        selected.isSelected = false;
        selected = kategorija;
    }
    //paginacija

    //najprej moramo izvedeti stevilo dogodkov

    porocilaIzkazService.kategorije.query({}, function(kategorije){
            $scope.kategorije = kategorije;
            getPodrobnosti(kategorije[0]);
            kategorije[0].isSelected = true;
            selected = kategorije[0];
           
    });

    $scope.izvoziPdf = function() {
        porocilaIzkazService.podrobno.query({}, function(izkazi) {
            var doc = new jsPDF();
            var textSize = 10;
            var padding = 5;
            var margin = 20;
            var a4end = 280;
            doc.setFontSize(textSize); 

            var y = margin;
            doc.text(margin, y, "Kategorija");
            doc.text(margin + 125, y, "Leto " + izkazi[0].zneski[0].leto);
            doc.text(margin + 155, y, "Leto " + izkazi[0].zneski[1].leto);
            y += 10;

            angular.forEach(izkazi, function(izkaz) {
                var kategorija = izkaz.kategorija.replace(/č/g, 'c').replace(/Č/g, 'C');

                doc.text(margin + 125, y, izkaz.zneski[0].znesek + "");
                doc.text(margin + 155, y, izkaz.zneski[1].znesek + "");
                if (kategorija.length >= 55) {
                    doc.text(margin, y, kategorija.substring(0, 54) + (/\s/.test(kategorija.charAt(54)) ? '' : '-'));
                    y += padding;
                    doc.text(margin, y, kategorija.substring(54, kategorija.length));
                } else {
                    doc.text(margin, y, kategorija);
                }

                y += padding;
                if (y >= a4end) {
                    doc.addPage();
                    y = margin;
                }
            });

            doc.save('izkazizida.pdf');
        });
    };

  };
} );