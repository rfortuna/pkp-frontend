﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija stanja aplikacije (id: 'namizje')
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './pregledController', 'text!./pregled.html'
], function ( strings, pregledController, pregledTemplate ) {
  return {
    id: 'pregled',
    url: strings.pregled.url,
    strings: strings.pregled,
    template: pregledTemplate,
    controller: pregledController,
    isSubPage: true,
    requiresAuth: true,
    reloadOnSearch: false
  };
} );
