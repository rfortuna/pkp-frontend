﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Metpodatki za podstran zemljevid.
 *
 * @ngdoc object
 */
define({
  root: {
    porocila: {
      url: '/porocila',
      title: 'Poročila',
      longTitle: 'Poročila',
      description: 'Poročila',
      keywords: 'pregled, struktura prometa, bilanca stanja, izkaz poslovnega izida'
    }
  }
} );
