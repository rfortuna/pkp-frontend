﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija podstrani zemljevid.
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './porocilaController', 'text!./porocila.html',
   './promet/states', './bilancaStanja/states', './izkazPoslovnegaIzida/states'
], function ( strings, porocilaController, porocilaTemplate,
		prometStates, bilancaStanjaStates, izkazPoslovnegaStanjaStates) {
  return {
    id: 'porocila',
    abstract: true,
    url: strings.porocila.url,
    strings: strings.porocila,
    template: porocilaTemplate,
    controller: porocilaController,
    isSubPage: true,
    icon: 'fa-bar-chart',
    children: [prometStates, bilancaStanjaStates, izkazPoslovnegaStanjaStates]
  };
} );
