﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v
 *   Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran zemljevid (zemljevid.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/porocila', 'lodash', 'jspdf', 'jspdf-tables'
], function ( strings, _ ) {

  return /*@ngInject*/ function ( $scope ) {
	  $scope.$strings = _.merge( {}, strings, $scope.$strings );
	  
	  var skupine = [
	           		{ime: 'Promet', open: true, 
	           			kategorije : [
	           			              { ime: "Pregled" , url: "home.porocila.pregled"},
	           			              { ime: "Struktura prometa", url: "home.porocila.strukturaprometa"}
	           			              ]},
	           		{ime: 'Bilanca stanja', open: false,
		            	kategorije : [
	           			              { ime: "Bilanca stanja", url: "porocila.bilancastanja"}
	           			              ]},
	           		{ime: 'Izkaz poslovnega izida', open:false,
		            	kategorije : [
	           			              { ime: "Bilanca stanja", url: "porocila.izkazposlovnegaizida"}
	           			              ]}       	  
	           	];
	  
	  $scope.skupine = skupine;
  }
}); 