﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Metapodatki za podstran namizje.
 *
 * @ngdoc object
 */
define({
  root: {
    strukturaPrometa: {
      url: '/strukturaPrometa',
      title: 'Struktura prometa',
      longTitle: 'Struktura prometa',
      description: 'Struktura prometa',
      keywords: 'promet, struktura'
    }
  }
} );
