/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Nizi za podstran namizje.
 *
 * @ngdoc object
 */
define({
  root: {
	  textNapovedPoslovana: "Napoved poslovanja",
	  textPregled: "Pregled",
	  textOpozoriloPremaloPodatkov: "Za izbrano obdobje ni dovolj podatkov za izris grafa!"
  }
} );
