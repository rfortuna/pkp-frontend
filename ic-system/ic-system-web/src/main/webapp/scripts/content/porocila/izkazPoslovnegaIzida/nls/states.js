﻿define({
  root: {
    izkazPoslovnegaIzida: {
      url: '/izkazPoslovnegaIzida',
      title: 'Izkaz poslovnega izida',
      longTitle: 'Izkaz poslovnega izida',
      description: 'Izkaz poslovnega izida.',
      keywords: 'pregled, transakcije'
    }
  }
} );
