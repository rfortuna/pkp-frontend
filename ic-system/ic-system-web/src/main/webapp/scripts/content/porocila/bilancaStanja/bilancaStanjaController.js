﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran namizje (namizje.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/bilancaStanja', 'lodash'
], function ( strings, _ ) {

  return /*@ngInject*/ function ( $scope, $filter, $sce, porocilaBilancaStanjaService) {

    $scope.$strings = _.merge( {}, strings, $scope.$strings );
    
    $scope.podatkiGrafa = {
    		labels: [],
    		datasets: []
    };
    
    var selected = {};
    
    $scope.$watch('podrobnostiKategorije.zneski.length', function(){
    	if($scope.podrobnostiKategorije == undefined) return;
    	labels = [];
    	datasets = [{
			label: "Vrednost kategorije",
			 fillColor: "rgba(151,187,205,0.5)",
	         strokeColor: "rgba(151,187,205,0.8)",
	         highlightFill: "rgba(151,187,205,0.75)",
	         highlightStroke: "rgba(151,187,205,1)",
            data: []
    	}];
    	
    	angular.forEach($scope.podrobnostiKategorije.zneski, function(obj){
    		labels.push(obj.leto);
    		datasets[0].data.push((obj.znesek));
    	});
    	    	
    	$scope.podatkiGrafa = {
    			labels : labels,
    			datasets: datasets
    	}
    	
    }); 

    $scope.kategorije = [];
    $scope.$watch('kategorije.length', function(){
        if($scope.kategorije.length == 0) {
            $scope.podrobnostiKategorije = $scope.$strings.textNiPodatkov;
        } else {
            getPodrobnosti($scope.kategorije[0]);
        }
    });
    
    
    var getPodrobnosti = function(kategorija) {
    	$scope.podrobnostiKategorije = porocilaBilancaStanjaService.kategorije.get({
			kategorijaId : kategorija.id
		});
    	
    }
    
    $scope.zamenjajPodrobnosti = function(kategorija) {
        getPodrobnosti(kategorija);
        kategorija.isSelected = true;
        selected.isSelected = false;
        selected = kategorija;
    }

    //paginacija

    //najprej moramo izvedeti stevilo dogodkov
    porocilaBilancaStanjaService.kategorije.query({}, function(kategorije){
        $scope.kategorije = kategorije;
        console.log(kategorije);
        getPodrobnosti(kategorije[0]);
        kategorije[0].isSelected = true;
        selected = kategorije[0];
    });

    $scope.izvoziPdf = function() {
        porocilaBilancaStanjaService.podrobno.query({}, function(bilance) {
            var doc = new jsPDF();
            var textSize = 10;
            var padding = 5;
            var margin = 20;
            var a4end = 280;
            doc.setFontSize(textSize); 

            var y = margin;
            doc.text(margin, y, "Kategorija");
            doc.text(margin + 125, y, "Leto 2013");
            doc.text(margin + 155, y, "Leto 2014");
            y += 10;


            angular.forEach(bilance, function(bilanca) {
                var kategorija = bilanca.kategorija.replace(/č/g, 'c').replace(/Č/g, 'C');

                doc.text(margin + 125, y, bilanca.zneski[0].znesek + "");
                doc.text(margin + 155, y, bilanca.zneski[1].znesek + "");
                if (kategorija.length >= 55) {
                    doc.text(margin, y, kategorija.substring(0, 54) + (/\s/.test(kategorija.charAt(54)) ? '' : '-'));
                    y += padding;
                    doc.text(margin, y, kategorija.substring(54, kategorija.length));
                } else {
                    doc.text(margin, y, kategorija);
                }

                y += padding;
                if (y >= a4end) {
                    doc.addPage();
                    y = margin;
                }
            });

            doc.save('bilancastanja.pdf');
        });
    };
    
    
  };
} );