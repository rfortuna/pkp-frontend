﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran namizje (namizje.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/strukturaPrometa', 'lodash'
], function ( strings, _ ) {

  return /*@ngInject*/ function ( $scope, porocilaStrukturaService) {

    $scope.$strings = _.merge( {}, strings, $scope.$strings );
    
    $scope.prihodkiData = [];
    $scope.odhodkiData = [];
    $scope.analizaData = [];
    
    $scope.responsePrihodki = null;
    $scope.responseOdhodki = null;
    $scope.responseAnaliza = null;
    
    var firstLoad = true;
    
    $scope.$watch("responsePrihodki.delezi.length", function(){
    	if($scope.responsePrihodki == undefined) return;
    	var chartData = $scope.obdelajPrihodkeOdhodkePieChart($scope.responsePrihodki, $scope.$strings.baseBarva1, $scope.$strings.baseBarva2);
    	$scope.prihodkiData = chartData;
    });
    
    $scope.$watch("responseOdhodki.delezi.length", function(){
    	if($scope.responsePrihodki == undefined) return;
    	var chartData = $scope.obdelajPrihodkeOdhodkePieChart($scope.responseOdhodki, $scope.$strings.baseBarva2, $scope.$strings.baseBarva1);
    	$scope.odhodkiData = chartData;
    });
    
    $scope.$watch("responseAnaliza.length", function(){
    	if($scope.responseAnaliza == undefined) return;

    	labels = [];
	    datasets = [
	        {
	            label: "Dejanska vrednost",
	            fillColor: "rgba(220,220,220,0.5)",
	            strokeColor: "rgba(220,220,220,0.8)",
	            highlightFill: "rgba(220,220,220,0.75)",
	            highlightStroke: "rgba(220,220,220,1)",
	            data: []
	        },
	        {
	            label: "Referenčna vrednost",
	            fillColor: "rgba(151,187,205,0.5)",
	            strokeColor: "rgba(151,187,205,0.8)",
	            highlightFill: "rgba(151,187,205,0.75)",
	            highlightStroke: "rgba(151,187,205,1)",
	            data: []
	        }
	    ];
    	
	    angular.forEach($scope.responseAnaliza, function(obj) {
	    	labels.push(obj.delez.stroskovniNosilec.sn);
	    	datasets[0].data.push(obj.delez.skupniZnesek);
	    	datasets[1].data.push(obj.referencnaVrednost);
	    });
	    
	    $scope.analizaData = {
    		labels : labels,
			datasets: datasets
	    }
	    
    });
    
    var loadPrihodki = function() {
    	$scope.responsePrihodki = porocilaStrukturaService.prihodki.get(
    			{odDatum: $scope.startDate.date.toISOString() , doDatum: $scope.endDate.date.toISOString() });  
    }
    
    var loadOdhodki = function() {
    	$scope.responseOdhodki = porocilaStrukturaService.odhodki.get(
    			{odDatum: $scope.startDate.date.toISOString() , doDatum: $scope.endDate.date.toISOString() });  
    }
    
    var loadAnaliza = function() {
    	$scope.responseAnaliza = porocilaStrukturaService.analiza.query(
    			{odDatum: $scope.startDate.date.toISOString() , doDatum: $scope.endDate.date.toISOString() });  
    }
    
  //datePickerTimeIni
    
    var dateChange = function() {
    	if(firstLoad) {
    		firstLoad = false;
    		return;
    	} 
    	loadPrihodki();
    	loadOdhodki();
    	loadAnaliza();
    }
    
	var today = new Date();
    $scope.endDate = {
    	date: today
    }
    
	var threeMonthBack = new Date();
	threeMonthBack.setDate(threeMonthBack.getMonth() - 90);
    $scope.startDate = {
    	date: threeMonthBack
    }
    
    $scope.$watch('startDate.date', dateChange);
    $scope.$watch('endDate.date', dateChange);
    
  };
} );