﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija stanja aplikacije (id: 'namizje')
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './strukturaPrometaController', 'text!./strukturaPrometa.html'
], function ( strings, strukturaPrometaController, strukturaPrometaTemplate ) {
  return {
    id: 'strukturaprometa',
    url: strings.strukturaPrometa.url,
    strings: strings.strukturaPrometa,
    template: strukturaPrometaTemplate,
    controller: strukturaPrometaController,
    isSubPage: true,
    requiresAuth: true,
    reloadOnSearch: false
  };
} );
