﻿define({
  root: {
    pregled: {
      url: '?datumOd&datumDo',
      title: 'Pregled',
      longTitle: 'Pregled',
      description: 'Pregled transakcij.',
      keywords: 'pregled, transakcije'
    }
  }
} );
