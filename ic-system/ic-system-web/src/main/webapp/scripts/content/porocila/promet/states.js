﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija stanja aplikacije (id: 'namizje')
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './prometController', 'text!./promet.html', './pregled/states', './strukturaPrometa/states'
], function ( strings, prometController, prometTemplate, pregledStates, strukturaPrometaStates) {
  return {
    id: 'promet',
    url: strings.promet.url,
    abstract: true,
    strings: strings.promet,
    template: prometTemplate,
    controller: prometController,
    isSubPage: true,
    icon:'fa-automobile',
    requiresAuth: true,
    children: [pregledStates, strukturaPrometaStates]
  };
} );
