﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija stanja aplikacije (id: 'namizje')
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './izkazPoslovnegaIzidaController', 'text!./izkazPoslovnegaIzida.html'
], function ( strings, izkazPoslovnegaIzidaController, izkazPoslovnegaIzidaTemplate ) {
  return {
    id: 'izkazPoslovnegaIzida',
    url: strings.izkazPoslovnegaIzida.url,
    strings: strings.izkazPoslovnegaIzida,
    template: izkazPoslovnegaIzidaTemplate,
    controller: izkazPoslovnegaIzidaController,
    isSubPage: true,
    icon: 'fa-money',
    requiresAuth: true,
    requiredRoles: ['direktor']
  };
} );
