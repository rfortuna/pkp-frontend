/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Nizi za podstran namizje.
 *
 * @ngdoc object
 */
define({
  root: {
	  textPrihodki: "Prihodki",
	  textOdhodki: "Odhodki",
	  textAnalizaPoslovanja: "Analiza poslovanja",
	  textOpozorilo: "Opozorilo",
	  textOpozoriloText: "Vasi stroski so za 67% od reprezentativnih vrednosti!",
	  textOpozoriloNasvet: "Kontaktirajte racunovodjo!"
  }
} );
