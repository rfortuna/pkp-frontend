﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Konfiguracija stanja aplikacije (id: 'namizje')
 *
 * @ngdoc object
 */
define([
  'i18n!./nls/states', './locationController', 'text!./location.html'
], function ( strings, locationController, locationTemplate) {
  return {
    id: 'location',
    url: strings.location.url,
    strings: strings.location,
    template: locationTemplate,
    controller: locationController,
    isSubPage: true,
    icon: 'fa-location-arrow'
  };
} );
