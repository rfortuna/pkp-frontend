﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Kontroler za podstran namizje (namizje.html).
 *
 * @ngdoc function
 * @scope
 */
define([
  'i18n!./nls/location', 'lodash', 'async!http://maps.google.com/maps/api/js?sensor=false'
], function ( strings, _ ) {

  return /*@ngInject*/ function ( $scope, $state, $stateParams) {
  	  		
    $scope.$strings = _.merge( {}, strings, $scope.$strings );
    
    $scope.map = { center: { latitude: 0, longitude: 0 }, zoom: 15, pan: true };
    $scope.marker = { center: { latitude: 0, longitude: 0 }, id: "one" };

    
    $scope.sendLocation = function() {
    	// send current user location to server
      console.log("GETLOKACIJA");
	  if (navigator.geolocation) {
		console.log("NAVIGATOR");
	    navigator.geolocation.getCurrentPosition($scope.showPosition);
	  }
	  
    }
    
    $scope.showPosition = function(position) {
      $scope.$apply(function(){
    	console.log(position.coords);
        $scope.map.center.latitude = position.coords.latitude;
        $scope.map.center.longitude = position.coords.longitude;
        $scope.marker.center.latitude = position.coords.latitude;
        $scope.marker.center.longitude = position.coords.longitude;
      });
    } 

  };
} );