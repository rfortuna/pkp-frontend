/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Incializacija gradnikov ogrodja spletne aplikacije.
 *
 * @ngdoc object
 */
define([
  'lodash', 'text!../content/_footer.html', 'text!../content/_header.html', 'text!../content/_left-panel.html',
  'text!../content/_ribbon.html'
], function ( _, footer, header, leftPanel,
              ribbon ) {

  var partials = {
    '_footer.html': footer,
    '_header.html': header,
    '_left-panel.html': leftPanel,
    '_ribbon.html': ribbon
  };

  var initialize = function ( $templateCache ) {
    _.forEach( partials, function ( part, key ) {
      $templateCache.put( key, part );
    } );
  };

  return {
    initialize: initialize
  };
} );