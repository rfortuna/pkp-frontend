/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Inicializira vse podane prestreznike.
 *
 * @ngdoc object
 */
define([
  'lodash', './auth', './errors'
], function ( _, auth, errors ) {

  var interceptors = {
    authInterceptor: auth,
    errorInterceptor: errors
  };

  var initialize = function ( angModule ) {
    _.forEach( interceptors, function ( interceptor, name ) {
      angModule.factory( name, interceptor );
    } );
  };

  return {
    initialize: initialize
  };
} );