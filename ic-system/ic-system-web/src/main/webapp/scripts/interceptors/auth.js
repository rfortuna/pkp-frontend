/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za ra�unalni�tvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Prestreznik za avtentikacijo.
 *
 * @ngdoc function
 */
define([], function () {

  return /*@ngInject*/ function ( $q, $rootScope ) {

    return {
      request: function ( config ) {
      	
      	config.headers['Authorization'] = 'Bearer ' + $rootScope.keycloak.token;
        var deferred = $q.defer();
        deferred.resolve(config);
        return deferred.promise;
      }
    };
  };
} );