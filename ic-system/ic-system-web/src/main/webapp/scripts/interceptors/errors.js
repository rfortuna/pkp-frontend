/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za ra�unalni�tvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Prestreznik za obvestila o genericnih napakah.
 *
 * @ngdoc function
 */
define([
  'lodash'
], function ( _ ) {

  return /*@ngInject*/ function ( $q, $utils, $rootScope, $injector ) {

    return {
      responseError: function ( rejection ) {
    	
    	if(rejection.status == 401){
    		$rootScope.keycloak.logout();
    	}else if (rejection.status != 400 ){
    		$injector.get('$state').go('domov.error', {errorCode: rejection.status});
    	}
        
        return $q.reject( rejection );
      },
      response: function(response){
        return response;
      }
      
    };
  };
} );