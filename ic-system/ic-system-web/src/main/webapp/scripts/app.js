﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Inicializacija osnovnih nastavitev spletne aplikacije, vstavljanje odvisnosti zunanjih modulov, zagon AngularJS aplikacije in pomembnih modulov, ki jih celotna aplikacija uporablja.
 *
 * @ngdoc object
 * @scope
 */
define([
  'angular', 'angular-ui-router','./routing/stateTree', 'models/models', 'interceptors/interceptors',
  'filters/filters', 'services/services', 'directives/directives', 'factories/factories', 'partials/partials',
  'i18n!content/nls/common', 'keycloak',

  'ui-bootstrap', 'angular-route', 'ngTable', 'angular-sanitize',
  'cookie-choices' , 'angular-resource', 'angular-maps', 'angular-local-storage'
], function ( angular, router, stateTree, models, interceptors,
              filters, services, directives, factories, partials,
              strings , Keycloak) {

  var initialize = function () {

    var keycloakContainer = {};

    var app = angular.module( 'template', [
      'ngRoute',
      'ui.router',
      'ui.bootstrap',
      'ngTable',
      'ngSanitize',
      'ngResource',
      'uiGmapgoogle-maps',
      'LocalStorageModule'
    ] );

    /*
     * SMART ADMIN GLOBAL CONFUGIRATION
     */

    //smart admin options
    $.enableJarvisWidgets = true;
    $.enableMobileWidgets = true;
    $.menu_speed = 235;
    $.navAsAjax = true;

    var body = $( 'body' );

    // Calculate nav height
    $.calc_navbar_height = function () {
      var height = null;
      var header = $( '#header' );

      if ( header.length )
        height = header.height();

      if ( height === null )
        height = $( '<div id="header"></div>' ).height();

      if ( height === null )
        return 49;
      return height;
    };
    $.navbar_height = $.calc_navbar_height();

    //smart admin variables
    $.root_ = body;
    $.left_panel = $( '#left-panel' );
    $.shortcut_dropdown = $( '#shortcut' );
    $.bread_crumb = $( '#ribbon').find('ol.breadcrumb' );

    // desktop or mobile
    $.device = null;

    var ismobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test( navigator.userAgent.toLowerCase() ));

    if ( !ismobile ) {
      $.root_.addClass( 'desktop-detected' );
      $.device = 'desktop';
    } else {
      $.root_.addClass( 'mobile-detected' );
      $.device = 'mobile';
    }

    if ( body.hasClass( 'menu-on-top' ) || localStorage.getItem( 'sm-setmenu' ) == 'top' ) {
      $topmenu = true;
      body.addClass( 'menu-on-top' );
    }

    /*
     * CUSTOM MENU PLUGIN
     */
    $.fn.extend( {

      jarvismenu: function ( options ) {

        var defaults = {
          accordion: 'true',
          speed: 200,
          closedSign: '[+]',
          openedSign: '[-]'
        };

        // Extend our default options with those provided.
        var opts = $.extend( defaults, options );
        //Assign current element to variable, in this case is UL element
        var $this = $( this );

        //add a mark [+] to a multilevel menu
        $this.find( 'li' ).each( function () {
          if ( $( this ).find( 'ul' ).size() !== 0 ) {
            //add the multilevel sign next to the link
            $( this ).find( 'a:first' ).append( '<b class="collapse-sign">' +
                                                ($( this ).hasClass( 'open' ) ? opts.openedSign : opts.closedSign) +
                                                '</b>' );

            //avoid jumping to the top of the page when the href is an #
            if ( $( this ).find( 'a:first' ).attr( 'href' ) == '' ) {
              $( this ).find( 'a:first' ).click( function ( event ) {
                event.preventDefault();
                return false;
              } );
            }
          }
        } );

        //open active level
        $this.find( 'li.active' ).each( function () {
          $( this ).parents( 'ul' ).slideDown( opts.speed );
          $( this ).parents( 'ul' ).parent( 'li' ).find( 'b:first' ).html( opts.openedSign );
          $( this ).parents( 'ul' ).parent( 'li' ).addClass( 'open' );
        } );

        $this.find( 'li a' ).click( function () {

          if ( $( this ).parent().find( 'ul' ).size() !== 0 ) {

            if ( opts.accordion ) {
              //Do nothing when the list is open
              if ( !$( this ).parent().find( 'ul' ).is( ':visible' ) ) {
                parents = $( this ).parent().parents( 'ul' );
                visible = $this.find( 'ul:visible' );
                visible.each( function ( visibleIndex ) {
                  var close = true;
                  parents.each( function ( parentIndex ) {
                    if ( parents[ parentIndex ] == visible[ visibleIndex ] ) {
                      close = false;
                      return false;
                    }
                  } );
                  if ( close ) {
                    if ( $( this ).parent().find( 'ul' ) != visible[ visibleIndex ] ) {
                      $( visible[ visibleIndex ] ).slideUp( opts.speed, function () {
                        $( this ).parent( 'li' ).find( 'b:first' ).html( opts.closedSign );
                        $( this ).parent( 'li' ).removeClass( 'open' );
                      } );
                    }
                  }
                } );
              }
            }
            if ( $( this ).parent().find( 'ul:first' ).is( ':visible' ) &&
                 !$( this ).parent().find( 'ul:first' ).hasClass( 'active' ) ) {
              $( this ).parent().find( 'ul:first' ).slideUp( opts.speed, function () {
                $( this ).parent( 'li' ).removeClass( 'open' );
                $( this ).parent( 'li' ).find( 'b:first' ).delay( opts.speed ).html( opts.closedSign );
              } );
            } else {
              $( this ).parent().find( 'ul:first' ).slideDown( opts.speed, function () {
                $( this ).parent( 'li' ).addClass( 'open' );
                $( this ).parent( 'li' ).find( 'b:first' ).delay( opts.speed ).html( opts.openedSign );
              } );
            }
          }
        } );
      }
    } );
    /* ~ END: CUSTOM MENU PLUGIN */
    /* ~ END: SMART ADMIN GLOBAL CONFIGURATION */

    var auth = {};

    app.config(
      function ( $stateProvider, $routeProvider, $urlRouterProvider, $locationProvider, $httpProvider, localStorageServiceProvider ) {
        $locationProvider.hashPrefix( '!' );
        $urlRouterProvider.when( '', '/domov' );
        stateTree.addStates( $stateProvider );
        $urlRouterProvider.otherwise( '/error/404' );
        $httpProvider.interceptors.push( 'authInterceptor' );
        $httpProvider.interceptors.push( 'errorInterceptor' );
        $httpProvider.defaults.headers.common.Accept = 'application/json, text/plain';
        $httpProvider.defaults.headers.common[ 'Content-Type' ] = 'application/json';
        $httpProvider.defaults.timeout = 15000;
      } );

    models.initialize( app );
    filters.initialize( app );
    services.initialize( app );
    factories.initialize( app );
    directives.initialize( app );
    interceptors.initialize( app );

    /** Modal controller **/
    app.controller('ModalInstanceCtrl', function($scope, $modalInstance, dogodki) {
      $scope.dismiss = function() {
        $modalInstance.dismiss('cancel');
      };

      $scope.dogodek = dogodki[0];
    });

    app.run( function ( $window, $rootScope, $state, $stateParams, $utils, $templateCache, $timeout, $http, localStorageService, googleService, lokacijaService, $modal) {
      $rootScope.$state = $state;
      $rootScope.$stateTree = stateTree;
      $rootScope.$stateParams = $stateParams;
      $rootScope.$strings = strings;

      $rootScope.$auth = auth;
      $rootScope.loadings = {};
      $rootScope.passErrors = [ 400, 404, 422, 401 ];

      partials.initialize( $templateCache );

      $rootScope.getStateUrl = function ( stateId, stateParams ) {
        return $state.href( stateId, stateParams ) === '' ? '#!' : $state.href( stateId, stateParams );
      };

      $rootScope.isActiveState = function ( stateid ) {
        if ( $rootScope.$state.current.hiddenSubPage ) {
          return $rootScope.$state.current.absoluteId.indexOf( stateid ) === 0;
        }
        else return ($rootScope.$state.current.absoluteId === stateid);
      };
      $rootScope.changeState = function ( stateid, stateParams ) {
        $rootScope.$state.go( stateid, stateParams );
      };
      $rootScope.hasActiveChild = function ( state ) {
        for ( var i = 0; i < state.children.length; i++ ) {
          var st = state.children[ i ];
          if ( st.children && st.children.length > 0 ) {
            if ( $rootScope.hasActiveChild( st ) ) return true;
          }
          else {
            if ( $rootScope.isActiveState( st.absoluteId ) ) return true;
          }
        }
        return false;
      };

      $rootScope.getAbsoluteUrl = function ( relativeUrl ) {
        if ( relativeUrl ) {
          return window.location.host + '/' + relativeUrl;
        }
      };

      $rootScope.isPrerendered = function () {
        return (navigator.userAgent.match( /(Prerender)/i ));
      };

      // Authentication and authorization

      $rootScope.$on( '$stateChangeStart',
        function ( event, toState, toParams ) {
          // Check for required authentication and roles
          // $broadcast 'noAuthentication' if auth fails
          // $broadcast 'noAuthorization' if roles check fails
    	  
    	  var allowed = !toState.requiredRoles ||
          _.every( toState.requiredRoles, function ( role ) {
            if($.inArray(role, $rootScope.keycloak.realmAccess.roles) > - 1 ){
              return true;
            }
          });
          
          if(!allowed) {
        	  $rootScope.$broadcast('noAuthorization');
          }
          
      });

      $rootScope.$on( 'noAuthentication', function ( event, url ) {

        // Handle no authentication (route to home, display error, ...)
      } );

      $rootScope.$on( 'noAuthorization', function () {
    	  $state.go('domov.error', {errorCode: 403});
        // Handle no roles (route to home, display error, ...)
      } );

      // Remove loading
      $('#main_loading').remove();

      // Cookie consent
      // cookieChoices.showCookieConsentBar( $rootScope.$strings.piskotki,
      //   $rootScope.$strings.zapriSp, $rootScope.$strings.izvediteVec, $rootScope.getStateUrl( 'domov.pomoc.varstvo' ) );
      // });
      
      //odhodkiPrihodki pie chart
      $rootScope.shadeColor = function (color, percent) {  
          var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
          return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (G<255?G<1?0:G:255)*0x100 + (B<255?B<1?0:B:255)).toString(16).slice(1);
      }
      
      $rootScope.obdelajPrihodkeOdhodkePieChart = function(data, baseBarva, highLightBarva) {
      	var chartData = [];
      	var ostali = {
  			value: 0,
  			color: null,
  			highlight: highLightBarva,
  			label: "Ostalo",
  			naziv: "Ostalo"
      	}
      	var i = 0;
      	angular.forEach(data.delezi, function(obj) {
      		if(obj.skupniZnesek > data.celotenZnesek * $rootScope.$strings.pieChartProcent) {
      			var naziv = obj.stroskovniNosilec.opis + " (" + obj.stroskovniNosilec.sn + ")"
      			chartData.push({
  				    value: obj.skupniZnesek,
  				    color: $rootScope.shadeColor(baseBarva, - i * $rootScope.$strings.pieChartKorakOsvetlitve),
  				    highlight: highLightBarva,
  				    label: obj.stroskovniNosilec.sn,
  				    naziv: naziv
      			});
      			i++;
      		}
      		else {
      			ostali.value += obj.skupniZnesek;
      		}
      	});
      	
      	ostali.color = $rootScope.shadeColor(baseBarva, - i * $rootScope.$strings.pieChartKorakOsvetlitve);
      	if(ostali.value != 0) {
      		chartData.push(ostali);
      	}
      	
      	return chartData;
      }
      
    $rootScope.obdelajIzdaniPrejetiLineChart = function(responseData) {
    	  
    	var labels = [];
      	var datasets = [{
  	    		label: "Izdani",
  	            fillColor: "rgba(220,220,220,0.2)",
  	            strokeColor: "rgba(220,220,220,1)",
  	            pointColor: "rgba(220,220,220,1)",
  	            pointStrokeColor: "#fff",
  	            pointHighlightFill: "#fff",
  	            pointHighlightStroke: "rgba(220,220,220,1)",
  	            data: []},
              {
  	            label: "Prejeti",
  	            fillColor: "rgba(151,187,205,0.2)",
  	            strokeColor: "rgba(151,187,205,1)",
  	            pointColor: "rgba(151,187,205,1)",
  	            pointStrokeColor: "#fff",
  	            pointHighlightFill: "#fff",
  	            pointHighlightStroke: "rgba(151,187,205,1)",
  	            data: []}
      	];
      	
      	for(i = 0; i < responseData.length; i++) {
      		var obj = responseData[i];
      		labels.push(obj.obdobje);
      		datasets[obj.tip=="izdani" ? 0 : 1].data.push(obj.dnevniZnesek);
      		if(i < responseData.length - 1) {
      			var objNaslednji = responseData[i+1];
      			if(objNaslednji.obdobje == obj.obdobje) {
      	    		datasets[objNaslednji.tip=="izdani" ? 0 : 1].data.push(objNaslednji.dnevniZnesek);
      	    		i++;
      			} else {
      	    		datasets[obj.tip=="izdani" ? 1 : 0].data.push(0);
      			}
      		} else {
      			datasets[obj.tip=="izdani" ? 1 : 0].data.push(0);
      		}
      	}
      	var dataGraph = { labels : labels, datasets: datasets};
    	
      	return dataGraph;
    }
            
//    $rootScope.drawLineGraph = function(dataContainer)  {
//  		if(dataContainer.chart == undefined) {
//  			dataContainer.chartCtx = $(dataContainer.canvasId).get(0).getContext('2d');
//  			dataContainer.chart = new Chart(dataContainer.chartCtx).Line(dataContainer.dataGraph);  			
//  			return dataContainer;
//  		} else {
//  			var html = $(dataContainer.canvasContainerId).html();
//  			$(dataContainer.canvasId).remove(); 
//  			$(dataContainer.canvasContainerId).append(html);
//  			dataContainer.chartCtx = $(dataContainer.canvasId).get(0).getContext('2d');
//  			dataContainer.chart = new Chart(dataContainer.chartCtx).Line(dataContainer.dataGraph);
//  			console.log(dataContainer.chart);
//			return dataContainer;
//  		}
//  	}
    $rootScope.drawLineGraph = function(dataContainer)  {
  		if(dataContainer.chart == undefined) {
  			dataContainer.chartCtx = $(dataContainer.canvasId).get(0).getContext('2d');
  			dataContainer.chart = new Chart(dataContainer.chartCtx).Line(dataContainer.dataGraph);  			
  			return dataContainer;
  		} else {
  			var height = $(dataContainer.canvasId).height();
  	    	var width = $(dataContainer.canvasId).width();
  			var html = $(dataContainer.canvasContainerId).html();
  			$(dataContainer.canvasId).remove(); 
  			$(dataContainer.canvasContainerId).append(html);
  			var canvas = document.querySelector(dataContainer.canvasId);
			dataContainer.chartCtx = canvas.getContext('2d');
			dataContainer.chartCtx.canvas.width = width;
			dataContainer.chartCtx.canvas.height = height; 
			dataContainer.chart = new Chart(dataContainer.chartCtx).Line(dataContainer.dataGraph);
			return dataContainer;
  		}
  	}

      
      //nastavi zaceten in koncen casovni interval
      
    //datePickerTimeInit
  	
  	
  	$rootScope.setStartEndDate = function(startDate, endDate, stateParams, success) {
  				
  		//koncni datum v url-ju
  		if(stateParams.datumDo != undefined) {
  			var newDate = new Date(stateParams.datumDo);
  			endDate = {
  				date: newDate
  			}
  		} else {
  			var today = new Date();
  		    endDate = {
  		    	date: today
  		    }
  		}
  				
  	    //zacetni datum v url-ju
  	    if(stateParams.datumOd != undefined) {
  			var newDate = new Date(stateParams.datumOd);
  	    	startDate = {
  	    		date: newDate
  	    	}
  	    } else {
  	    	//dva meseca od koncnega datuma
  	    	var oneMonthBack = new Date();
  	    	oneMonthBack.setDate(oneMonthBack.getMonth() - 60);
  	        startDate = {
  	        	date: oneMonthBack
  	        }
  	    }
  	    
  	    success(startDate, endDate);
  	}

    $rootScope.keycloak = keycloakContainer;
    
    $rootScope.getGoogleRacun = function() {
      googleService.googleRacun.get({}, function(racun) {
        $rootScope.googleRacun = racun;
        console.log(racun);

        if (racun.accessToken != undefined) {
          repeatCall();
        }

      });
    };
    $rootScope.getGoogleRacun();

    $rootScope.connectGoogle = function() {
      if ($rootScope.googleRacun == undefined || $rootScope.googleRacun.accessToken == undefined) {
        $window.location.href = 'https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=47021962903-8geg2dr1dv4r9d4jinuv1dtk0k3o5n9j.apps.googleusercontent.com&redirect_uri=http://localhost:8080/test.html&scope=https://www.googleapis.com/auth/calendar.readonly&access_type=offline&state=security_token';
      } else {
        googleService.googleRevoke.revoke({token: $rootScope.googleRacun.accessToken}, function(response) {
          googleService.googleDisconnect.remove({}, function(response) {
            $rootScope.getGoogleRacun();
          });
        });
      }
    };
    
    //timeout function    
    var timeout = 1000 * 60 * 1;
    var moveDelta = 10; // 10 meters min movement

    // izračunaj razdaljo med dvema točkama v metrih (aproksimacija)
    var locationDistanceKM = function(lat1,lon1,lat2,lon2) {
      var R = 6371; // radij zemlje
      var dLat = deg2rad(lat2-lat1);  // deg2rad spodaj
      var dLon = deg2rad(lon2-lon1); 
      var a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2)
        ; 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c * 1000; // razdalja v metrih
      return d;
    }

    // pretvori stopinje v radiane
    var deg2rad = function(deg) {
      return deg * (Math.PI/180);
    }
    
    //preveri ce se je uporabnik dovolj premaknil
    var positionChangeTooSmall = function(curPosition) {
      // za testiranje
      return false;

      if (localStorageService.isSupported) {
        var latOld = localStorageService.get("lastLatitude");
        var lonOld = localStorageService.get("lastLongitude");

        return locationDistanceKM(latOld, lonOld, curPosition.latitude, curPosition.longitude) <= moveDelta;
      }

    	return true;
    }
    
    var reportMyLocation = function(position) {
      var myLocation = {
        latitude:  position.coords.latitude,
        longitude:  position.coords.longitude
      };

    	if(localStorageService.isSupported) {
    		var lastTime;
    		if(lastTime = localStorageService.get("lastTime")) {
    			var currentTime = new Date();
    			if(currentTime - lastTime < timeout || positionChangeTooSmall()) {
    				return;
    			}
    			
    		}
    	}
    	
      lokacijaService.reportLocation.insert(myLocation, function(data) {
        if(localStorageService.isSupported) {
          var now = new Date();
          localStorageService.set("lastTime", now.getTime());
          localStorageService.set("lastLatitude", myLocation.latitude);
          localStorageService.set("lastLongitude", myLocation.longitude);
        }

        if (data != undefined && data.length > 0) {
            var modalInstance = $modal.open({
              templateUrl: 'scripts/content/modal.html',
              controller: 'ModalInstanceCtrl',
              resolve: {
                dogodki: function () {
                  return data;
                }
              }
            });
        }
      });
    }
    
    var getCurrentUserLocation = function(){
    	if (navigator.geolocation) {
    	    navigator.geolocation.getCurrentPosition(reportMyLocation);
    	  }
    }
    
    var repeatCall = function() {
    	getCurrentUserLocation();
    	$timeout(repeatCall, timeout);
    }
  
    });

    //keycloak avtentikacija
    keycloakContainer = Keycloak();
    keycloakContainer.init({ onLoad: 'login-required' })
      .success(function(){
        angular.bootstrap( document, [ 'template' ] );
    });

  }

  return {
    initialize: initialize
  };
} );
