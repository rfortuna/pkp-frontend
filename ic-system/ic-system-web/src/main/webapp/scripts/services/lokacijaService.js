define( ['./serviceUrl'], function (serviceUrl) {
    return function($resource) {
        return {
            reportLocation: $resource(serviceUrl.url + 'lokacija', {}, {
                insert: {method: 'POST', isArray: true}
            })
    	}
    };
} );