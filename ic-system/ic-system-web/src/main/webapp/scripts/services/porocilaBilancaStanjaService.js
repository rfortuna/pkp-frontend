define( ['./serviceUrl'], function (serviceUrl) {
    return function($resource) {
        return {
        	kategorije: $resource( serviceUrl.url +'porocila/bilanca/:kategorijaId',
            {
            	kategorijaId : "@id"
            }, {}),
            kategorijeStevilo: $resource(serviceUrl.url + 'porocila/bilanca/stevilo', {}),
            podrobno: $resource(serviceUrl.url + 'porocila/bilanca/podrobno', {})
	     }
    };
} );