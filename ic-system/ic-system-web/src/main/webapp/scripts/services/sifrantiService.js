define( ['./serviceUrl'], function (serviceUrl) {
    return function($resource) {
    	return	{
    		drzave:  $resource(serviceUrl.url +'lokacija/drzave', {}),
    		mesta: $resource(serviceUrl.url + 'lokacija/drzave/:id/poste')
    	}
    };
} );