define( ['./serviceUrl'], function (serviceUrl) {
    return function($resource) {
        return {
        	googleRacun: $resource(serviceUrl.url + 'koledar/googleAuth', {}),
            googleConnect: $resource(serviceUrl.url + 'koledar/googleAuth', {}, {
                insert: {method: 'POST'}
            }),
            googleDisconnect: $resource(serviceUrl.url + 'koledar/googleAuth', {}, {
                remove: {method: 'DELETE'}
            }),
            googleRevoke: $resource('https://accounts.google.com/o/oauth2/revoke?callback=JSON_CALLBACK', {}, {revoke: {method: 'JSONP'}})
    	}
    };
} );