define( ['./serviceUrl'], function (serviceUrl) {
    return function($resource) {
        return {
        	abecedni: {
                stranke: $resource(
			            serviceUrl.url +'stranke', {}),
                stevilo: $resource(serviceUrl.url + 'stranke/stevilo',{})
            },
            dolzniki:{
                stranke: $resource(
                        serviceUrl.url +'stranke/dolzniki', {}),
                stevilo: $resource(serviceUrl.url + 'stranke/dolzniki/stevilo',{})
            },
            stalne:{
                stranke: $resource(
                        serviceUrl.url +'stranke/stalne', {}),
                stevilo: $resource(serviceUrl.url + 'stranke/stalne/stevilo',{})
            },
            stranka: $resource(serviceUrl.url + 'stranke/:id',
                        {
                          id : '@id'
                        },{}),
            strankaTransakcije: {
                stevilo: $resource(serviceUrl.url + 'stranke/:id/transakcije/stevilo',
                        {
                          id : '@id'
                        },{}),
                transakcije: $resource(serviceUrl.url + 'stranke/:id/transakcije',
                        {
                          id : '@id'
                        },{})
            },
            strankaGraf: $resource(serviceUrl.url + 'stranke/:id/transakcije/graf',
            		{
            			id : '@id'
            		}, {}),
            strankaLokacija: $resource(serviceUrl.url + 'stranke/:strankaId/naslov',{
	            	strankaId: '@strankaId'
	            }, { 
	            	update: {method: 'PUT'}
	            })
            
    	}
    };
} );