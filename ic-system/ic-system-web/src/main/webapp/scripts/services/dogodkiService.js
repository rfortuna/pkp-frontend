define( ['./serviceUrl'], function (serviceUrl) {
    return function($resource) {
        return {
        	dogodki: $resource(
			            serviceUrl.url +'dogodki/:dogodekId',
			            {
			              dogodekId : '@id'
			            }, {}),
            nasvet: $resource(
			            serviceUrl.url +'nasvet/', {}),
            dogodkiStevilo: $resource(serviceUrl.url + 'dogodki/stevilo', {})

    	}
    };
} );