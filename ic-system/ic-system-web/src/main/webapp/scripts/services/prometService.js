define( ['./serviceUrl'], function (serviceUrl) {
    return function($resource) {
    	return	{
    		stanje:  $resource(serviceUrl.url +'promet/stanje', {}),
    		prihodki: {
                transakcije: $resource(serviceUrl.url + 'promet/prihodki'),
                stevilo: $resource(serviceUrl.url + 'promet/prihodki/stevilo')
            },
    		odhodki: { 
                transakcije: $resource(serviceUrl.url + 'promet/odhodki'),
                stevilo: $resource(serviceUrl.url + 'promet/odhodki/stevilo')
            },
    		podrobnostiRacuna: $resource(serviceUrl.url + 'promet/:id', {
    			id: "@id",
    		})
    	}
    };
} );