define( ['./serviceUrl'], function (serviceUrl) {
    return function($resource) {
    	return {
    		pregled: $resource( serviceUrl.url +'porocila/pregled',
    	            {}, {}),
    	    napoved: $resource( serviceUrl.url +'porocila/pregled/napoved',
    	            {}, {}),
    	}
	           
    };
} );
