﻿/**
 * @name SOA in koncept oblaka storitev na primeru sistema RIJS
 * @author Laboratorij za integracijo informacijskih sistemov, Fakulteta za računalništvo in informatiko Univerze v Ljubljani
 * @copyright LIIS, FRI, 2014
 *
 * @description Inicializacija podanih seviceov.
 *
 * @ngdoc object
 */
define([
  'lodash', './dogodkiService', './porocilaPrometPregledService', './porocilaBilancaStanjaService', './porocilaIzkazService',
  './porocilaStrukturaService', './prometService', './strankeService', './sifrantiService', './googleService', './lokacijaService'
], function ( _, dogodkiService, porocilaPrometPregledService, porocilaBilancaStanjaService, porocilaIzkazService,
		porocilaStrukturaService, prometService, strankeService, sifrantiService, googleService, lokacijaService) {

  var services = {
		  dogodkiService: dogodkiService,
		  porocilaPrometPregledService: porocilaPrometPregledService,
		  porocilaBilancaStanjaService: porocilaBilancaStanjaService,
		  porocilaIzkazService: porocilaIzkazService,
		  porocilaStrukturaService: porocilaStrukturaService,
		  prometService: prometService,
	      strankeService: strankeService,
	      sifrantiService: sifrantiService,
        googleService: googleService,
        lokacijaService: lokacijaService

  };

  var initialize = function ( angModule ) {
    _.forEach( services, function ( service, name ) {
      angModule.service( name, service );
    } );
  };

  return {
    initialize: initialize
  };
} );