define( ['./serviceUrl'], function (serviceUrl) {
    return function($resource) {
        return { 
        	kategorije: $resource(
            serviceUrl.url +'porocila/izkaz/:kategorijaId',
            {
            	kategorijaId : "@id"
            }, {}),
            kategorijeStevilo: $resource(serviceUrl.url + 'porocila/izkaz/stevilo', {}),
            podrobno: $resource(serviceUrl.url + 'porocila/izkaz/podrobno', {})
	    } 
    };
} );