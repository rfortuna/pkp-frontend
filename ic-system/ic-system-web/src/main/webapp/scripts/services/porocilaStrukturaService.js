define( ['./serviceUrl'], function (serviceUrl) {
    return function($resource) {
    	return {
    		prihodki: $resource( serviceUrl.url +'porocila/struktura/prihodki',
    	            {}, {}),
    	    odhodki: $resource( serviceUrl.url +'porocila/struktura/odhodki',
    	            {}, {}),
            analiza: $resource( serviceUrl.url +'porocila/struktura/analiza',
    	            {}, {})
    	            
    	}
	           
    };
} );
